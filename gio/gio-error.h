/* 
 * Gio Library: gio/gio-error.h
 *
 * Copyright (C) 2005 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * A structure for stream errors.
 */

#ifndef __GIO_ERROR_H__
#define __GIO_ERROR_H__ 1

#include <glib-object.h>

G_BEGIN_DECLS

#define GIO_ERROR(ptr) \
  ((GioError *) (ptr))

typedef enum
{
  GIO_ERROR_DEBUG,
  GIO_ERROR_INFO,
  GIO_ERROR_WARN,
  GIO_ERROR_CRITICAL,
  GIO_ERROR_FATAL
}
GioErrorLevel;

typedef struct _GioError GioError;
struct _GioError
{
  GError error;
  GioErrorLevel level;
};


GType     gio_error_get_type      (void) G_GNUC_CONST;

GioError *gio_error_new           (GioErrorLevel   level,
				   GQuark          domain,
				   gint            code,
				   const gchar    *format,
				   ...) G_GNUC_PRINTF (4, 5);
GioError *gio_error_new_literal   (GioErrorLevel   level,
				   GQuark          domain,
				   gint            code,
				   const gchar    *message);
GioError *gio_error_copy          (GioError       *error);
void      gio_error_free          (GioError       *error);

void      gio_set_error           (GioError      **error,
				   GioErrorLevel   level,
				   GQuark          domain,
				   gint            code,
				   const gchar    *format,
				   ...) G_GNUC_PRINTF (5, 6);

GSList   *gio_error_list_copy     (GSList          *error_list);
void      gio_error_list_free     (GSList          *error_list);

G_END_DECLS

#endif /* !__GIO_ERROR_H__ */
