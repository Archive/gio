/* 
 * Gio Library: gio/gio-event.h
 *
 * Copyright (C) 2005 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * A structure for stream element events.
 */

#ifndef __GIO_EVENT_H__
#define __GIO_EVENT_H__ 1

#include <glib-object.h>

G_BEGIN_DECLS

#define GIO_TYPE_EVENT \
  (gio_event_get_type ())
#define GIO_IS_EVENT(ptr) \
  (((ptr) != NULL) && \
   ((*((GioEventType *) (ptr)) == GIO_EVENT_STATE) || \
    (*((GioEventType *) (ptr)) == GIO_EVENT_READABLE) || \
    (*((GioEventType *) (ptr)) == GIO_EVENT_WRITABLE) || \
    (*((GioEventType *) (ptr)) == GIO_EVENT_EOF) || \
    (*((GioEventType *) (ptr)) == GIO_EVENT_ERROR)))

typedef enum /* <prefix=GIO_EVENT,flags> */
{
  GIO_EVENT_NONE     = 0,
  GIO_EVENT_STATE    = 1 << 0,
  GIO_EVENT_READABLE = 1 << 1,
  GIO_EVENT_WRITABLE = 1 << 2,
  GIO_EVENT_EOF      = 1 << 3,
  GIO_EVENT_ERROR    = 1 << 4
}
GioEventType;

typedef union _GioEvent GioEvent;
typedef struct _GioEventAny GioEventAny;
typedef struct _GioEventState GioEventState;
typedef struct _GioEventReadable GioEventReadable;
typedef struct _GioEventWritable GioEventWritable;
typedef struct _GioEventEof GioEventEof;
typedef struct _GioEventError GioEventError;

struct _GioEventAny
{
  GioEventType type;
  gint64 timestamp;
};

struct _GioEventState
{
  GioEventType type;
  gint64 timestamp;
  gint state;
};

struct _GioEventReadable
{
  GioEventType type;
  gint64 timestamp;
};

struct _GioEventWritable
{
  GioEventType type;
  gint64 timestamp;
};

struct _GioEventEof
{
  GioEventType type;
  gint64 timestamp;
};

struct _GioEventError
{
  GioEventType type;
  gint64 timestamp;
  GSList *error_list;
};

union _GioEvent
{
  GioEventType type;

  GioEventAny any;
  GioEventState state;
  GioEventReadable readable;
  GioEventWritable writable;
  GioEventEof eof;
  GioEventError error;
};

GType     gio_event_get_type (void) G_GNUC_CONST;

GioEvent *gio_event_new           (GioEventType  type);
GioEvent *gio_event_new_with_time (GioEventType  type,
				   gint64        timestamp);
GioEvent *gio_event_copy          (GioEvent     *src);
void      gio_event_free          (GioEvent     *event);

G_END_DECLS

#endif /* !__GIO_EVENT_H__ */
