/* 
 * Gio Library: gio/gio-module.c
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, read to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#include <libxml/xmlreader.h>

#include <gmodule.h>

#include "gio-module.h"


/* *************** *
 *  Documentation  *
 * *************** */

/**
 * GioModuleInitFunc:
 * @type_module: the module in question.
 * 
 * The function prototype for a @type_module's initialization function. Modules
 * should implement a module_init() function using this prototype to register
 * their #GType<!-- -->s with the type system.
 * 
 * Since: 1.0
 **/

/**
 * GioModuleFinalizeFunc:
 * @type_module: the module in question.
 * 
 * The function prototype for a @type_module's finalization function. Modules
 * should implement a module_finalize() function using this prototype to clean
 * up after themselves when they are to be unloaded from memory.
 * 
 * Since: 1.0
 **/

/**
 * GioModuleListTypesFunc:
 * @n_types: a location of a variable to store the number of types provided by
 *  a module.
 * @types: a location of a constant array to store the types provided by a
 *  module.
 * 
 * The function prototype for a module's type listing function. Modules should
 * implement a module_list_types() function using this prototype to identify
 * to the Gio library what #GType<!-- -->s they provide.
 * 
 * Since: 1.0
 **/

/**
 * GioModule:
 * 
 * The instance structure for GioModule objects. This structure contains no
 * public members.
 * 
 * Since: 1.0
 **/

/**
 * GIO_MODULE:
 * @object: the pointer to cast.
 * 
 * Casts the #GioModule derived pointer at @object into a (GioModule*) pointer.
 * Depending on the current debugging level, this function may invoke certain
 * runtime checks to identify invalid casts.
 * 
 * Since: 1.0
 **/

/**
 * GIO_IS_MODULE:
 * @object: the pointer to check.
 * 
 * Checks whether @object is a #GioModule (or derived) instance.
 * 
 * Since: 1.0
 **/

/**
 * GIO_TYPE_MODULE:
 * 
 * The registered #GType of the #GioModule class.
 * 
 * Since: 1.0
 **/


/* **************** *
 *  Private Macros  *
 * **************** */

#define GIO_MODULES_FILE "gio-modules.xml"
#define GIO_RNG_FILENAME "gio-modules.rng"


/* ******************** *
 *  Private Structures  *
 * ******************** */

typedef struct _GioModuleClass GioModuleClass;

struct _GioModule
{
  GTypeModule parent;

  GModule *library;
  gchar *name;

  GioModuleInitFunc init;
  GioModuleFinalizeFunc finalize;
  GioModuleListTypesFunc list_types;
};

struct _GioModuleClass
{
  GTypeModuleClass parent_class;
};


/* ********************* *
 *  Function Prototypes  *
 * ********************* */

/* GTypeModule Functions */
static gboolean     gio_module_load               (GTypeModule  *type_module);
static void         gio_module_unload             (GTypeModule  *type_module);

/* Utility Functions */
static GioModule   *get_module_locked             (const gchar  *name);

static inline void  parse_modules_index           (gint          fd,
						   const gchar  *filename);
static inline void  ensure_uptodate_modules_index (void);


/* ****************** *
 *  Global Variables  *
 * ****************** */

G_LOCK_DEFINE_STATIC (loaded_modules);

static GHashTable *loaded_modules = NULL;
static GHashTable *modules_type_name_index = NULL;
static time_t index_mtime = -1;


/* ******************* *
 *  GType Declaration  *
 * ******************* */

G_DEFINE_TYPE (GioModule, gio_module, G_TYPE_TYPE_MODULE);


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gio_module_class_init (GioModuleClass *class)
{
  GObjectClass *gobject_class;
  GTypeModuleClass *type_module_class;

  gobject_class = G_OBJECT_CLASS (class);
  type_module_class = G_TYPE_MODULE_CLASS (class);

  type_module_class->load = gio_module_load;
  type_module_class->unload = gio_module_unload;

  G_LOCK (loaded_modules);

  if (!loaded_modules)
    loaded_modules = g_hash_table_new_full (g_str_hash, g_str_equal, g_free,
					    g_object_unref);

  if (!modules_type_name_index)
    modules_type_name_index = g_hash_table_new_full (g_str_hash, g_str_equal,
						     xmlFree, xmlFree);

  G_UNLOCK (loaded_modules);
}

static void
gio_module_init (GioModule *module)
{
}


/* *********************** *
 *  GTypeModule Functions  *
 * *********************** */

static gboolean
gio_module_load (GTypeModule *type_module)
{
  GioModule *module;
  gchar *basename, *module_path;

  module = GIO_MODULE (type_module);

  basename = g_strconcat ("lib", module->name, "." G_MODULE_SUFFIX, NULL);
  module_path = g_build_filename (GIO_MODULES_DIR, basename, NULL);
  g_free (basename);

  module->library = g_module_open (module_path, 0);
  g_free(module_path);
  if (!module->library)
    {
      g_warning (g_module_error());
      return FALSE;
    }

  if (!g_module_symbol (module->library, "module_init",
			(gpointer *) &module->init) ||
      !g_module_symbol (module->library, "module_finalize", 
			(gpointer *) &module->finalize) ||
      !g_module_symbol (module->library, "module_list_types", 
			(gpointer *) &module->list_types))
    {
      g_warning (g_module_error());
      g_module_close (module->library);
      return FALSE;
    }

  (*module->init) (type_module);

  return TRUE;
}

static void
gio_module_unload (GTypeModule *type_module)
{
  GioModule *module;

  module = GIO_MODULE (type_module);

  (*module->finalize) (type_module);

  g_module_close (module->library);
  module->library = NULL;

  module->init = NULL;
  module->finalize = NULL;
  module->list_types = NULL;
}


/* ****************** *
 *  Utiity Functions  *
 * ****************** */

static GioModule *
get_module_locked (const gchar *name)
{
  GioModule *retval;

  retval = g_hash_table_lookup (loaded_modules, name);

  if (!retval)
    {
      retval = g_object_new (GIO_TYPE_MODULE, NULL);
      g_type_module_set_name (G_TYPE_MODULE (retval), name);
      retval->name = g_strdup (name);
      g_hash_table_insert (loaded_modules, g_strdup (name),
			   retval);
    }

  if (!g_type_module_use (G_TYPE_MODULE (retval)))
    return NULL;

  return retval;
}

static inline void
parse_modules_index (gint         fd,
		     const gchar *filename)
{
  xmlTextReaderPtr reader;
  int result;
  gchar *rng_filename;
  xmlChar *module_id;

  filename = g_build_filename (GIO_CACHE_DIR, GIO_MODULES_FILE, NULL);
  reader = xmlReaderForFd (fd, filename, "UTF-8", 0);

  if (!reader)
    {
      g_warning ("Unable to open Gio modules index at `%s`.", filename);
      return;
    }

  rng_filename = g_build_filename (GIO_DATA_DIR, GIO_RNG_FILENAME, NULL);
  result = xmlTextReaderRelaxNGValidate (reader, rng_filename);
  if (result != 0)
    g_warning ("Unable to process Gio modules schema at `%s'.", rng_filename);

  g_free (rng_filename);

  module_id = NULL;
  result = xmlTextReaderRead (reader);
  while (result == 1)
    {
      const xmlChar *element;
      xmlChar *id;

      element = xmlTextReaderConstLocalName (reader);

      if (g_ascii_strcasecmp ((const gchar *) element, "module") == 0)
	{
	  xmlFree (module_id);
	  id = xmlTextReaderGetAttribute (reader, (const xmlChar *) "xml:id");

	  if (id)
	    {
	      module_id = xmlStrdup (id);
	      xmlFree (id);
	      id = NULL;
	    }
	  else
	    {
	      g_warning ("Invalid <module> tag at %s:%d:%d: Missing xml:id attribute.",
			 filename, xmlTextReaderGetParserLineNumber (reader),
			 xmlTextReaderGetParserColumnNumber (reader));
	      result = xmlTextReaderNext (reader);
	      continue;
	    }
	}
      else if (g_ascii_strcasecmp ((const gchar *) element, "gtype") == 0)
	{
	  id = xmlTextReaderGetAttribute (reader, (const xmlChar *) "xml:id");
	
	  if (id)
	    {
	      if (module_id)
		g_hash_table_insert (modules_type_name_index, id,
				     xmlStrdup (module_id));
	      else
		xmlFree (id);
      
	      id = NULL;
	    }
	  else
	    {
	      g_warning ("Invalid <type> tag at %s:%d:%d: Missing xml:id attribute.",
			 filename, xmlTextReaderGetParserLineNumber (reader),
			 xmlTextReaderGetParserColumnNumber (reader));
	      result = xmlTextReaderNext (reader);
	      continue;
	    }
	}

      result = xmlTextReaderRead (reader);
    }

  xmlTextReaderClose (reader);
}

static inline void
ensure_uptodate_modules_index (void)
{
  gchar *filename;
  struct stat st;
  gint fd;

  filename = g_build_filename (GIO_CACHE_DIR, GIO_MODULES_FILE, NULL);
  
  errno = 0;
  fd = open (filename, 0);

  if (fd >= 0)
    {
      if (fstat (fd, &st) != 0 || st.st_mtime != index_mtime)
	{
	  index_mtime = st.st_mtime;
	  parse_modules_index (fd, filename);
	}

      close (fd);
    }
  else
    {
      g_warning ("Unable to open Gio modules index at `%s`: %s.", filename,
		 g_strerror (errno));
    }
}


/* ************************************************************************** *
 *  PUBLIC API                                                                *
 * ************************************************************************** */

/**
 * gio_module_get:
 * @name: the name of the module to load.
 * 
 * Retrieves an object representing the Gio plugin module called @name.
 * 
 * Returns: a pointer to a #GioModule object which must not be unreferenced.
 * 
 * Since: 1.0
 **/
GioModule *
gio_module_get (const gchar *name)
{
  GioModule *retval;
  
  g_return_val_if_fail (name != NULL && name[0] != '\0', NULL);

  G_LOCK (loaded_modules);

  if (!loaded_modules)
    loaded_modules = g_hash_table_new_full (g_str_hash, g_str_equal,
					    g_free, g_object_unref);

  if (!modules_type_name_index)
    modules_type_name_index = g_hash_table_new_full (g_str_hash, g_str_equal,
						     g_free, g_free);

  retval = get_module_locked (name);

  G_UNLOCK (loaded_modules);

  return retval;
}

/**
 * gio_module_get_for_type:
 * @type_name: the name of the type to load the module for.
 * 
 * Loads the module that contains the @type_name #GType.
 * 
 * Returns: a pointer to a #GioModule object which must not be unreferenced.
 * 
 * Since: 1.0
 **/
GioModule *
gio_module_get_for_type (const gchar *type_name)
{
  GioModule *retval;
  gchar *module_name;

  g_return_val_if_fail (type_name != NULL && type_name[0] != '\0', NULL);

  G_LOCK (loaded_modules);

  if (!loaded_modules)
    loaded_modules = g_hash_table_new_full (g_str_hash, g_str_equal,
					    g_free, g_object_unref);

  if (!modules_type_name_index)
    modules_type_name_index = g_hash_table_new_full (g_str_hash, g_str_equal,
						     g_free, g_free);

  ensure_uptodate_modules_index ();

  module_name = g_hash_table_lookup (modules_type_name_index, type_name);

  if (module_name)
    retval = get_module_locked (module_name);
  else
    retval = NULL;

  G_UNLOCK (loaded_modules);

  return retval;
}

/**
 * gio_module_list_types:
 * @module: the module to query.
 * 
 * Retrieves a newly-allocated list of string names for the types in @module.
 * 
 * Returns: a list of strings, both of which must be freed when no longer
 *  needed.
 * 
 * Since: 1.0
 **/
GSList *
gio_module_list_types (GioModule *module)
{
  GSList *retval;
  GType *types;
  guint n_types;

  g_return_val_if_fail (GIO_IS_MODULE (module), NULL);

  if (!g_type_module_use (G_TYPE_MODULE (module)))
    return NULL;

  retval = NULL;
  types = NULL;
  n_types = 0;

  (*module->list_types) (&n_types, &types);

  while (n_types)
    {
      retval = g_slist_prepend (retval,
				g_strdup (g_type_name (types[n_types - 1])));
      n_types--;
    }

  g_type_module_unuse (G_TYPE_MODULE (module));

  return retval;
}
