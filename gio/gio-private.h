/* 
 * Gio Library: gio/gio-private.h
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GIO_PRIVATE_H__
#define __GIO_PRIVATE_H__ 1

#include <glib-object.h>

#define GIO_BUG_REPORT_URI "mailto:jcape@ignore-your.tv"

/* Macro to check an enum value against a registered type */
#define GIO_ENUM_IS_VALID(en,en_T) (_gio_enum_is_valid ((en), (en_T)))

#define _gio_enum_is_valid(en,en_T) ({ \
  GEnumClass *__klass = g_type_class_ref ((en_T)); \
  gboolean __r; \
  __r = (g_enum_get_value (__klass, (en)) != NULL); \
  g_type_class_unref (__klass); \
  __r; \
})

/* Macro to check a bitwise flags value against a registered type */
#define GIO_FLAGS_IS_VALID(en,en_T) (_gio_flags_is_valid ((en), (en_T)))

#define _gio_flags_is_valid(en,en_T) ({ \
  GFlagsClass *__klass = g_type_class_ref ((en_T)); \
  gboolean __r; \
  __r = (g_flags_get_first_value (__klass, en) != NULL); \
  g_type_class_unref (__klass); \
  __r; \
})


#ifndef G_TYPE_ERROR
# define GIO_TYPE_GERROR (_gio_gerror_get_type ())
  GType _gio_gerror_get_type (void) G_GNUC_CONST;
#else
# define GIO_TYPE_GERROR G_TYPE_ERROR
#endif /* G_TYPE_ERROR */


#endif /* __GIO_PRIVATE_H__ */
