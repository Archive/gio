/* 
 * Gio Library: gio/gio-object.c
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gio-object.h"


/* *************** *
 *  Documentation  *
 * *************** */

/**
 * GioObject:
 * 
 * The instance structure for GioObject objects. This structure contains no
 * public members.
 * 
 * Since: 1.0
 **/

/**
 * GioObjectClass:
 * @context_changed: the C closure for the "context-changed" signal.
 * 
 * The class structure for GioObject objects.
 * 
 * Since: 1.0
 **/

/**
 * GIO_OBJECT_LOCK:
 * @object: the object to lock.
 * 
 * Acquires the thread-safety lock for @object. The thread-safety lock should be
 * acquired when attempting to use a stream, the main exception is when you are
 * attempting to use a stream object from within one of it's own signals. The
 * lock must be released with GIO_OBJECT_UNLOCK() when it is no longer needed.
 * 
 * Since: 1.0
 **/

/**
 * GIO_OBJECT_UNLOCK:
 * @object: the object to lock.
 * 
 * Releases the thread-safety lock for @object. The thread-safety lock should be
 * released when you are done using @object.
 * 
 * Since: 1.0
 **/

/**
 * GIO_OBJECT_TRYLOCK:
 * @object: the object to lock.
 * 
 * Attempts to acquire the thread-safety lock for @object. If the lock can be
 * acquired immediately, %TRUE will be returned and the lock will be acquired,
 * and you must release this lock with GIO_OBJECT_UNLOCK() when no longer
 * required; if the lock could not be acquired immediately, %FALSE will be
 * returned.
 * 
 * Since: 1.0
 **/

/**
 * GIO_OBJECT:
 * @object: the pointer to cast.
 * 
 * Casts the #GioObject derived pointer at @object into a (GioObject*) pointer.
 * Depending on the current debugging level, this function may invoke certain
 * runtime checks to identify invalid casts.
 * 
 * Since: 1.0
 **/

/**
 * GIO_IS_OBJECT:
 * @object: the pointer to check.
 * 
 * Checks whether @object is a #GioObject (or derived) instance.
 * 
 * Since: 1.0
 **/

/**
 * GIO_OBJECT_CLASS:
 * @klass: the pointer to cast.
 * 
 * Casts a the #GioObjectClass derieved pointer at @klass into a
 * (GioObjectClass*) pointer.
 * 
 * Since: 1.0
 **/

/**
 * GIO_IS_OBJECT_CLASS:
 * @klass: the pointer to check.
 * 
 * Checks whether @klass is a #GioObjectClass (or derived) class.
 * 
 * Since: 1.0
 **/

/**
 * GIO_OBJECT_GET_CLASS:
 * @object: the pointer to check.
 * 
 * Retrieves a pointer to the #GioObjectClass for the #GioObject (or derived)
 * instance, @object.
 * 
 * Since: 1.0
 **/

/**
 * GIO_TYPE_OBJECT:
 * 
 * The registered #GType of the #GioObject class.
 * 
 * Since: 1.0
 **/


/* **************** *
 *  Private Macros  *
 * **************** */

#define GIO_OBJECT_GET_PRIVATE(object)	(GIO_OBJECT (object)->priv)


/* ********************** *
 *  Private Enumerations  *
 * ********************** */

enum
{
  PROP_0,
  PROP_CONTEXT
};

enum
{
  CONTEXT_CHANGED,
  LAST_SIGNAL
};


/* ******************** *
 *  Private Structures  *
 * ******************** */

struct _GioObjectPrivate
{
  GMainContext *context;
};


/* ******************** *
 *  Private Prototypes  *
 * ******************** */

static void gio_object_set_property    (GObject      *object,
					guint         param_id,
					const GValue *value,
					GParamSpec   *pspec);
static void gio_object_get_property    (GObject      *object,
					guint         param_id,
					GValue       *value,
					GParamSpec   *pspec);
static void gio_object_finalize        (GObject      *object);


/* ****************** *
 *  Global Variables  *
 * ****************** */

static guint gio_object_signals[LAST_SIGNAL] = { 0 };


/* ******************* *
 *  GType Declaration  *
 * ******************* */

G_DEFINE_ABSTRACT_TYPE (GioObject, gio_object, G_TYPE_OBJECT);


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gio_object_class_init (GioObjectClass *class)
{
  GObjectClass *gobject_class;
  
  gobject_class = G_OBJECT_CLASS (class);
  
  gobject_class->set_property = gio_object_set_property;
  gobject_class->get_property = gio_object_get_property;
  gobject_class->finalize = gio_object_finalize;

/**
 * GioObject::context-changed:
 * @object: the object which emitted this signal.
 * @old_context: the old main loop context of the stream.
 * 
 * This signal is emitted by objects after they have changed their main loop context.
 * 
 * Since: 1.0
 **/
  gio_object_signals[CONTEXT_CHANGED] =
    g_signal_new ("context-changed",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_FIRST,
		  G_STRUCT_OFFSET (GioObjectClass, context_changed),
		  NULL, NULL, g_cclosure_marshal_VOID__BOXED,
		  G_TYPE_NONE, 1, GIO_TYPE_GMAIN_CONTEXT);

  g_object_class_install_property (gobject_class, PROP_CONTEXT,
				   g_param_spec_boxed ("context", "Context",
						       "The main loop this object is associated "
						       "with.",
						       GIO_TYPE_GMAIN_CONTEXT,
						       G_PARAM_READWRITE));

  g_type_class_add_private (class, sizeof (GioObjectPrivate));
}

static void
gio_object_init (GioObject *object)
{
  object->priv = G_TYPE_INSTANCE_GET_PRIVATE (object, GIO_TYPE_OBJECT, GioObjectPrivate);

  g_static_mutex_init (&(object->mutex));
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gio_object_set_property (GObject      *object,
			 guint         param_id,
			 const GValue *value,
			 GParamSpec   *pspec)
{
  switch (param_id)
    {
    case PROP_CONTEXT:
      gio_object_set_context (GIO_OBJECT (object), g_value_get_boxed (value));
      break;
  
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
      break;
    }
}

static void
gio_object_get_property (GObject    *object,
			 guint       param_id,
			 GValue     *value,
			 GParamSpec *pspec)
{
  GioObjectPrivate *priv;

  priv = GIO_OBJECT_GET_PRIVATE (object);

  switch (param_id)
    {
    case PROP_CONTEXT:
      g_value_set_boxed (value, priv->context);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
      break;
    }
}

static void
gio_object_finalize (GObject *object)
{
  GioObjectPrivate *priv;

  priv = GIO_OBJECT_GET_PRIVATE (object);

  if (priv->context != NULL)
    g_main_context_unref (priv->context);

  if (G_OBJECT_CLASS (gio_object_parent_class)->finalize != NULL)
    (*G_OBJECT_CLASS (gio_object_parent_class)->finalize) (object);
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

/**
 * gio_object_get_context:
 * @object: the object to examine.
 * 
 * Retrieves the main loop context this object is associated with.
 * 
 * Returns: a pointer to the main loop context used by @object. The returned
 *  pointer should not be modified or freed.
 * 
 * Since: 1.0
 **/
GMainContext *
gio_object_get_context (GioObject *object)
{
  g_return_val_if_fail (GIO_IS_OBJECT (object), NULL);

  return object->priv->context;
}


/** 
 * gio_object_set_context:
 * @object: the object to modify.
 * @context: the new main loop context.
 * 
 * Changes the main loop context used by @object to @context. The
 * "context-changed" signal will be emitted, and users of @object's current
 * context should take appropriate action from that signal's handler.
 * 
 * Since: 1.0
 **/
void
gio_object_set_context (GioObject    *object,
			GMainContext *context)
{
  GMainContext *old_context;

  g_return_if_fail (GIO_IS_OBJECT (object));

  if (context == g_main_context_default ())
    context = NULL;

  old_context = object->priv->context;
  if (context == old_context)
    return;

  if (context)
    g_main_context_ref (context);
  object->priv->context = context;

  g_signal_emit (object, gio_object_signals[CONTEXT_CHANGED], 0, old_context);

  if (old_context)
    g_main_context_unref (object->priv->context);

  g_object_notify (G_OBJECT (object), "context");
}


static gpointer
ref_main_context (gpointer ctx)
{
  g_main_context_ref (ctx);

  return ctx;
}


GType
gio_gmain_context_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      type = g_boxed_type_register_static ("GMainContext",
					   ref_main_context,
					   (GBoxedFreeFunc) g_main_context_unref);
    }

  return type;
}
