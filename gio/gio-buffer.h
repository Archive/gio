/* 
 * Gio Library: gio/gio-buffer.h
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, read to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * A structure wrapping a data buffer.
 */

#ifndef __GIO_BUFFER_H__
#define __GIO_BUFFER_H__ 1

#include "gio-object.h"

G_BEGIN_DECLS

#define GIO_TYPE_BUFFER		\
  (gio_buffer_get_type ())
#define GIO_BUFFER(data) \
  ((GioBuffer *)(data))
#define GIO_IS_BUFFER(data) \
  (data != NULL && (*((GType *)(data))) == GIO_TYPE_BUFFER)

typedef struct _GioBuffer GioBuffer;
  
typedef void  (*GioBufferFreeFunc)      (gpointer   data,
					 gsize      length,
					 gsize      size,
					 gpointer   user_data);

GType          gio_buffer_get_type      (void) G_GNUC_CONST;

GioBuffer     *gio_buffer_new           (void);
GioBuffer     *gio_buffer_dup           (GioBuffer          *buffer);
void           gio_buffer_free          (GioBuffer          *buffer);

void           gio_buffer_copy          (GioBuffer          *src,
					 GioBuffer          *dest);
void           gio_buffer_merge         (GioBuffer          *dest,
					 gssize              dest_position,
					 GioBuffer          *src,
					 gssize              src_position,
					 gssize              n_bytes);
void           gio_buffer_clear         (GioBuffer          *buffer);

gconstpointer  gio_buffer_get_data      (GioBuffer          *buffer,
					 gsize              *size,
					 gsize              *length);
gpointer       gio_buffer_steal_data    (GioBuffer          *buffer,
					 gsize              *size,
					 gsize              *length);

void           gio_buffer_set_data      (GioBuffer          *buffer,
					 gconstpointer       data,
					 gssize              size,
					 gssize              length);
void           gio_buffer_take_data     (GioBuffer          *buffer,
					 gpointer            data,
					 gssize              length,
					 gssize              size);
void           gio_buffer_map_data      (GioBuffer          *buffer,
					 gpointer            data,
					 gssize              size,
					 gssize              length,
					 GioBufferFreeFunc   free_func,
					 gpointer            user_data,
					 GDestroyNotify      notify);

void           gio_buffer_insert        (GioBuffer          *buffer,
					 gssize              position,
					 gconstpointer       data,
					 gssize              length);
void           gio_buffer_append        (GioBuffer          *buffer,
					 gconstpointer       data,
					 gssize              length);
void           gio_buffer_prepend       (GioBuffer          *buffer,
					 gconstpointer       data,
					 gssize              length);
void           gio_buffer_remove        (GioBuffer          *buffer,
					 gssize              position,
					 gssize              n_bytes);
void           gio_buffer_overwrite     (GioBuffer          *buffer,
					 gssize              position,
					 gconstpointer       data,
					 gssize              length);
void           gio_buffer_memset        (GioBuffer          *buffer,
					 gssize              position,
					 guint8              value,
					 gssize              n_bytes);

gsize          gio_buffer_get_size      (GioBuffer          *buffer);
void           gio_buffer_set_size      (GioBuffer          *buffer,
					 gsize               size);
gsize          gio_buffer_get_length    (GioBuffer          *buffer);

G_END_DECLS

#endif /* !__GIO_BUFFER_H__ */
