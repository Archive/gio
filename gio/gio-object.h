/* 
 * Gio Library: gio/gio-object.h
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * An abstract base class for threadsafe objects which can be bound to
 * a particular GMainContext.
 */

#ifndef __GIO_OBJECT_H__
#define __GIO_OBJECT_H__ 1

#include <glib-object.h>

G_BEGIN_DECLS


#define GIO_TYPE_OBJECT \
  (gio_object_get_type ())
#define GIO_OBJECT(object) \
  (G_TYPE_CHECK_INSTANCE_CAST ((object), GIO_TYPE_OBJECT, GioObject))
#define GIO_IS_OBJECT(object) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((object), GIO_TYPE_OBJECT))
#define GIO_OBJECT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GIO_TYPE_OBJECT, GioObjectClass))
#define GIO_IS_OBJECT_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GIO_TYPE_OBJECT))
#define GIO_OBJECT_GET_CLASS(object) \
  (G_TYPE_INSTANCE_GET_CLASS ((object), GIO_TYPE_OBJECT, GioObjectClass))

#define GIO_OBJECT_LOCK(object) \
  (g_static_mutex_lock (&(GIO_OBJECT (object)->mutex)))
#define GIO_OBJECT_UNLOCK(object) \
  (g_static_mutex_unlock (&(GIO_OBJECT (object)->mutex)))
#define GIO_OBJECT_TRYLOCK(object) \
  (g_static_mutex_trylock (&(GIO_OBJECT (object)->mutex)))


#define GIO_TYPE_GMAIN_CONTEXT \
  (gio_gmain_context_get_type ())


typedef struct _GioObject GioObject;
typedef struct _GioObjectClass GioObjectClass;
typedef struct _GioObjectPrivate GioObjectPrivate;


struct _GioObject
{
  /* <private> */
  GObject parent;

  GStaticMutex mutex;

  GioObjectPrivate *priv;
};

struct _GioObjectClass
{
  /* <private> */
  GObjectClass parent_class;

  /* Methods */
  void (*__gio_reserved1);
  void (*__gio_reserved2);
  void (*__gio_reserved3);
  void (*__gio_reserved4);

  /* <public> */
  /* Signals */
  void (*context_changed) (GioObject    *object,
			   GMainContext *old_context);
  /* <private> */
  void (*__gio_reserved5);
  void (*__gio_reserved6);
  void (*__gio_reserved7);
  void (*__gio_reserved8);
};


GType         gio_object_get_type    (void) G_GNUC_CONST;

GMainContext *gio_object_get_context (GioObject    *object);
void          gio_object_set_context (GioObject    *object,
				      GMainContext *context);

GType     gio_gmain_context_get_type (void) G_GNUC_CONST;

G_END_DECLS

#endif /* !__GIO_OBJECT_H__ */
