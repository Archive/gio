/*
 * Gio Library: gio/gio-event.c
 *
 * Copyright (c) 2003-2005 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <glib/gi18n.h>

#include <sys/time.h>
#include <time.h>

#include "gio-error.h"
#include "gio-event.h"

GType
gio_event_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    type = g_boxed_type_register_static ("GioEvent",
					 (GBoxedCopyFunc) gio_event_copy,
					 (GBoxedFreeFunc) gio_event_free);

  return type;
}

/**
 * gio_event_new:
 * @type: the type of event to create.
 * 
 * Allocates a new event structure of @type, who's timestamp is set to the
 * milliseconds since the epoch.
 * 
 * Returns: a newly allocate event structure which must be freed with
 *  gio_event_free() when no longer needed.
 * 
 * Since: 1.0
 **/
GioEvent *
gio_event_new (GioEventType type)
{
  struct timeval tv;
    
  if (gettimeofday (&tv, NULL) == 0)
    return gio_event_new_with_time (type, tv.tv_usec);

  return gio_event_new_with_time (type, -1);
}

/**
 * gio_event_new_with_time:
 * @type: the type of event to create.
 * @timestamp: the timestamp of this event.
 * 
 * Allocates a new event structure of @type, with the given @timestamp.
 * 
 * Returns: a newly allocate event structure which must be freed with
 *  gio_event_free() when no longer needed.
 * 
 * Since: 1.0
 **/
GioEvent *
gio_event_new_with_time (GioEventType type,
			 gint64       timestamp)
{
  GioEvent *retval;

  retval = g_new0 (GioEvent, 1);
  retval->type = type;
  retval->any.timestamp = timestamp;

  return retval;
}

/**
 * gio_event_copy:
 * @src: the event to copy.
 * 
 * Duplicates (deep copies) the contents of @src into a new event structure.
 * 
 * Returns: a newly allocate event structure which must be freed with
 *  gio_event_free() when no longer needed.
 * 
 * Since: 1.0
 **/
GioEvent *
gio_event_copy (GioEvent *src)
{
  GioEvent *retval;

  if (!src || src->type == GIO_EVENT_NONE)
    return NULL;

  retval = gio_event_new_with_time (src->type, src->any.timestamp);

  switch (src->type)
    {
    case GIO_EVENT_STATE:
      retval->state.state = src->state.state;
      break;

    case GIO_EVENT_ERROR:
      retval->error.error_list = gio_error_list_copy (src->error.error_list);
      break;

    case GIO_EVENT_READABLE:
    case GIO_EVENT_WRITABLE:
    case GIO_EVENT_EOF:
      break;

    case GIO_EVENT_NONE:
    default:
      g_assert_not_reached ();
      break;
    }

  return retval;
}

/**
 * gio_event_free:
 * @event: the event to destroy.
 * 
 * Frees the given @event structure and it's contents.
 * 
 * Since: 1.0
 **/
void
gio_event_free (GioEvent *event)
{
  if (!event)
    return;

  switch (event->type)
    {
    case GIO_EVENT_STATE:
    case GIO_EVENT_READABLE:
    case GIO_EVENT_WRITABLE:
    case GIO_EVENT_EOF:
      break;

    case GIO_EVENT_ERROR:
      gio_error_list_free (event->error.error_list);
      break;

    default:
    case GIO_EVENT_NONE:
      g_critical ("%s: (%s): Invalid event type (%d), freeing anyways.",
		  G_STRLOC, G_STRFUNC, event->type);
      break;
    }

  g_free (event);
}
