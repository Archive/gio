/* 
 * Gio Library: gio/gio-module.h
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, read to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * Module-handling API.
 */

#ifndef __GIO_MODULE_H__
#define __GIO_MODULE_H__ 1

#include "gio-stream.h"

G_BEGIN_DECLS

#define GIO_TYPE_MODULE \
  (gio_module_get_type ())
#define GIO_MODULE(object) \
  (G_TYPE_CHECK_INSTANCE_CAST ((object), GIO_TYPE_MODULE, GioModule))
#define GIO_IS_MODULE(object) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((object), GIO_TYPE_MODULE))

typedef struct _GioModule GioModule;
  
typedef void (*GioModuleInitFunc)          (GTypeModule  *type_module);
typedef void (*GioModuleFinalizeFunc)      (GTypeModule  *type_module);
typedef void (*GioModuleListTypesFunc)     (guint        *n_types,
					    GType       **types);

GType      gio_module_get_type             (void) G_GNUC_CONST;

GioModule *gio_module_get                  (const gchar  *name);
GioModule *gio_module_get_for_type         (const gchar  *type_name);

GSList    *gio_module_list_types           (GioModule    *module);

G_END_DECLS

#endif /* !__GIO_MODULE_H__ */
