/*
 * Gio Library: gio/gio-stream.c
 *
 * Copyright (c) 2003, 2004 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <glib/gi18n.h>

#include "gio-error.h"
#include "gio-private.h"
#include "gio-module.h"
#include "gio-marshal.h"
#include "gio-type-builtins.h"
#include "gio-stream.h"

/* *************** *
 *  Documentation  *
 * *************** */

/**
 * GioStreamState:
 * @GIO_STREAM_STATE_CLOSING: the stream is in the process of shutting down.
 * @GIO_STREAM_STATE_CLOSED: the stream is shut down.
 * @GIO_STREAM_STATE_OPENING: the stream is in the process of opening.
 * @GIO_STREAM_STATE_OPEN: the stream is open and ready.
 * @GIO_STREAM_STATE_FROZEN: the stream is frozen for modification.
 * @GIO_STREAM_STATE_FIRST: the highest enumeration value which subclasses can
 *  use for pre-closed states.
 * @GIO_STREAM_STATE_LAST: the lowest enumeration value which subclasses can use
 *  for post-open states.
 * 
 * An enumeration of the common states a #GioStream can be in.
 * 
 * Since: 1.0
 **/

/**
 * GioStreamFlags:
 * @GIO_STREAM_TOPLEVEL: the stream object cannot have a parent.
 * @GIO_STREAM_READABLE: the stream object supports the gio_stream_read()
 *  method.
 * @GIO_STREAM_READ_BUFFERED: the stream object supports the gio_stream_peek()
 *  method.
 * @GIO_STREAM_WRITABLE: the stream object supports the gio_stream_write()
 *  method.
 * @GIO_STREAM_WRITE_BUFFERED: the stream object buffers it's writing, and
 *  supports the gio_stream_flush() method.
 * @GIO_STREAM_SEEKABLE: the stream object supports the gio_stream_seek() and
 *  gio_stream_tell() methods.
 * 
 * A bitwise enumeration of the capabilities of a stream object.
 * 
 * Since: 1.0
 **/

/**
 * GioStreamError:
 * @GIO_STREAM_ERROR_UNKNOWN: the stream encounted a totally unexpected error.
 * 
 * An enumeration of common IO errors.
 * 
 * Since: 1.0
 **/

/**
 * GioStream:
 * 
 * The instance structure for GioStream objects. This structure contains no
 * public members.
 * 
 * Since: 1.0
 **/

/**
 * GioStreamClass:
 * @get_flags: the vtable method called by gio_stream_get_flags() to determine
 *  this stream's capabilities.
 * @get_size: the vtable method called by gio_stream_get_size() to determine
 *  this stream's size.
 * @freeze: the vtable method called once by gio_stream_freeze() to freeze an
 *  object when it's freeze count is one.
 * @thaw: the vtable method called once by gio_stream_thaw() to thaw an object
 *  when it's freeze count drops back to zero.
 * @open: the vtable method called by gio_stream_open() to open the stream
 *  synchronously.
 * @close: the vtable method called by gio_stream_close() to close the stream.
 * @read: the vtable method called by gio_stream_read() to read data from this
 *  stream.
 * @peek: the vtable method called by gio_stream_peek() to examine pending data
 *  on this stream.
 * @write: the vtable method called by gio_stream_write() to write data to this
 *  stream.
 * @flush: the vtable method called by gio_stream_flush() to flush any written,
 *  buffered data.
 * @seek: the vtable method called by gio_stream_seek() to move the stream
 *  offset.
 * @tell: the vtable method called by gio_stream_tell() to retrieve the current
 *  stream offset.
 * @parent_changed: the C closure for the "parent-changed" signal.
 * @readable: the C closure for the "readable" signal.
 * @writable: the C closure for the "writable" signal.
 * 
 * The class structure for GioStream objects.
 * 
 * Since: 1.0
 **/

/**
 * GioStreamGetFlagsFunc:
 * @stream: the stream in question.
 * 
 * The function prototype for an implementation of the gio_stream_get_flags()
 * method. All subclasses of GioStream must implement the get_flags() method to
 * retrieve a static indication of what functionality the subclass supports.
 * 
 * <note>This method is <emphasis>not</emphasis> parent-relative.</note>
 * 
 * <example><title>A get_flags() implementation</title><programlisting>static GioStreamFlags
 * my_stream_get_flags (GioStream *stream)
 * {
 *   return (GIO_STREAM_READABLE | GIO_STREAM_WRITABLE);
 * }</programlisting></example>
 * 
 * Returns: a bitwise enumeration of @stream's capabilities.
 * 
 * Since: 1.0
 **/

/**
 * GioStreamGetSizeFunc:
 * @stream: the stream in question.
 * 
 * The function prototype for an implementation of the gio_stream_get_size()
 * method. All subclasses of GioStream must implement the get_size() method to
 * retrieve the current size (in bytes) of a stream. If a stream does not
 * support pre-determined sizes (like a socket, or certain parsing stream), then
 * this function should return %-1.
 * 
 * <note>This method is <emphasis>not</emphasis> parent-relative</note>
 * 
 * <example><title>A get_size() implementation for a metadata stream</title>
 * <programlisting>static gint64
 * my_mime_stream_get_size (GioStream *stream)
 * {
 *   return gio_stream_get_size (gio_stream_get_parent (stream));
 * }</programlisting></example>
 * 
 * <example><title>A get_size() implementation for a standard parsing stream</title>
 * <programlisting>static gint64
 * my_parsing_stream_get_size (GioStream *stream)
 * {
 *   return -1;
 * }</programlisting></example>
 * 
 * Returns: the size in bytes of the source of @stream, or %-1.
 * 
 * Since: 1.0
 **/

/**
 * GioStreamFreezeFunc:
 * @stream: the stream in question.
 * 
 * The function prototype for an implementation of the gio_stream_freeze()
 * method. All subclasses of GioStream must implmement the freeze() method to
 * "pause" a stream to allow for modification of settings and/or a shuffling of
 * a stream stack.
 * 
 * Implementations will be called once, when an object is initially frozen.
 * Subsequent calls to gio_stream_freeze() will simply increase the "freeze
 * count."
 * 
 * <note>This method <emphasis>is</emphasis> parent-relative.</note>
 * 
 * Since: 1.0
 **/

/**
 * GioStreamThawFunc:
 * @stream: the stream in question.
 * 
 * The function prototype for an implementation of the gio_stream_thaw() method.
 * All subclasses of GioStream must implement the thaw() method to "unpause" or
 * "resume" a stream after being frozen to pick things up where it left off.
 * 
 * Implementations will be called once, when an object's "freeze count" has
 * dropped to zero.
 * 
 * <note>This method <emphasis>is</emphasis> parent-relative.</note>
 * 
 * Since: 1.0
 **/

/**
 * GioStreamOpenFunc:
 * @stream: the stream in question.
 * @error: a location to store any returned errors in.
 * 
 * The function prototype for an implementation of the gio_stream_open()
 * method. All subclasses of GioStream must implement the open() method to
 * open a stream and prepare it for operation synchronously.
 * 
 * <example><title>An open() implementation for a protocol stream</title>
 * <programlisting>static gboolean
 * my_protocol_stream_open (GioStream  *stream,
 *                          GError    **error)
 * {
 *   GioStream *parent;
 *   GError *tmp_error;
 *   gboolean retval;
 *   <!-- -->
 *   parent = gio_stream_get_parent (stream);
 *   tmp_error = NULL;
 *   <!-- -->
 *   retval = gio_stream_open (parent, &amp;tmp_error);
 *   if (retval)
 *     {
 *       gio_stream_update_state (stream, MY_PROTOCOL_LOGGING_IN, tmp_error);
 *       <!-- -->
 *       retval = do_handshake (stream, &amp;tmp_error);
 *       if (!retval)
 *         gio_stream_update_state (stream, GIO_STREAM_STATE_ERROR, tmp_error);
 *       else
 *         gio_stream_update_state (stream, GIO_STREAM_STATE_OPEN, NULL);
 *       <!-- -->
 *     }
 *   else
 *     {
 *       gio_stream_update_state (stream, GIO_STREAM_STATE_ERROR, tmp_error);
 *     }
 *   <!-- -->
 *   if (error)
 *     *error = tmp_error;
 *   else if (tmp_error)
 *     g_error_free (tmp_error);
 *   <!-- -->
 *   return retval;
 * }</programlisting></example>
 * 
 * Returns: %TRUE if the stream is open and ready, %FALSE if an error occurred.
 * 
 * Since: 1.0
 **/

/**
 * GioStreamStartOpenFunc:
 * @stream: the stream in question.
 * 
 * The function prototype for an implementation of the gio_stream_start_open()
 * method. All subclasses of GioStream must implement the start_open() method to
 * start the process to open a stream asynchronously.
 * 
 * <example><title>A start_open() implementation for a protocol stream</title>
 * <programlisting>static void
 * wait_for_open (GioStream    *parent,
 *                gint          state,
 *                const GError *error,
 *                gpointer      data)
 * {
 *   GioStream *stream;
 *   <!-- -->
 *   stream = GIO_STREAM (data);
 *   <!-- -->
 *   switch (gio_stream_get_state (parent))
 *     {
 *     case GIO_STREAM_STATE_OPEN:
 *       handle_open (stream);
 *       gio_stream_update_state (stream, GIO_STREAM_STATE_OPEN, NULL);
 *       break;
 *     case GIO_STREAM_STATE_ERROR:
 *       gio_stream_update_state (stream, GIO_STREAM_STATE_ERROR, error);
 *       break;
 *     }
 * }
 * <!-- -->
 * static void
 * my_protocol_start_open (GioStream *stream)
 * {
 *   GioStream *parent;
 *   <!-- -->
 *   parent = gio_stream_get_parent (stream);
 *   <!-- -->
 *   g_signal_connect (parent, "state-changed", G_CALLBACK (wait_for_open),
 *                     stream);
 *   gio_stream_start_open (parent);
 * }</programlisting></example>
 * 
 * Since: 1.0
 **/

/**
 * GioStreamCloseFunc:
 * @stream: the stream object in question.
 * @error: a pointer to a location to store errors in.
 * 
 * The functon prototype for an implementation of the gio_stream_close()
 * method. All subclasses of #GioStream must implement a close method to
 * actually shut down the stream.
 * 
 * Returns: %TRUE if the stream has been successfully closed, %FALSE if it has
 *  not.
 * 
 * Since: 1.0
 **/

/**
 * GioStreamReadFunc:
 * @stream: the stream in question.
 * @buffer: a pointer to a location to store a returned buffer object.
 * @read_size: the maximum size to read.
 * @error: a pointer to a location to store errors.
 * 
 * The function prototype for an implementation of the gio_stream_read() method.
 * Subclasses which indicated they are readable via the gio_stream_get_flags()
 * method must implement this function to actually perform reads.
 * 
 * 
 * 
 * Returns: %TRUE if the read was successful, %FALSE if it was not.
 * 
 * Since: 1.0
 **/

/**
 * GioStreamReadFunc:
 * @stream: the stream in question.
 * @buffer: a pointer to a location to store a returned buffer object.
 * @read_size: the maximum size to read.
 * @error: a pointer to a location to store errors.
 * 
 * The function prototype for an implementation of the gio_stream_read() method.
 * Subclasses which indicated they are readable via the gio_stream_get_flags()
 * method must implement this function to actually perform reads.
 * 
 * 
 * 
 * Returns: %TRUE if the read was successful, %FALSE if it was not.
 * 
 * Since: 1.0
 **/

/**
 * GioStreamFunc:
 * @stream: the stream in question.
 * @user_data: user data to pass to this function.
 * 
 * A function prototype for a generic #GioStream callback.
 * 
 * Returns: %TRUE if this callback should remain installed, %FALSE if it should
 *  be removed.
 * 
 * Since: 1.0
 **/

/**
 * GIO_STREAM_IS_TOPLEVEL:
 * @stream: the #GioStream<!-- -->-derived object in to test.
 * 
 * Tests whether @stream is a toplevel stream, and thus cannot have any parents.
 * 
 * Since: 1.0
 **/

/**
 * GIO_STREAM_IS_READABLE:
 * @stream: the #GioStream<!-- -->-derived object to examine.
 * 
 * Tests whether @stream is readable, and supports the "readable" signal
 * and the gio_stream_read(), gio_stream_start_read(), and
 * gio_stream_stop_read() methods.
 * 
 * Since: 1.0
 **/

/**
 * GIO_STREAM_IS_READ_BUFFERED:
 * @stream: the #GioStream<!-- -->-derived object to examine.
 * 
 * Tests whether @stream uses a buffered read, and supports the
 * gio_stream_peek() method.
 * 
 * Since: 1.0
 **/

/**
 * GIO_STREAM_IS_WRITABLE:
 * @stream: the #GioStream<!-- -->-derived object to examine.
 * 
 * Tests whether @stream is writable, and supports the "writable" signal
 * and the gio_stream_write(), gio_stream_start_write(), and
 * gio_stream_stop_write() methods.
 * 
 * Since: 1.0
 **/

/**
 * GIO_STREAM_IS_WRITE_BUFFERED:
 * @stream: the #GioStream<!-- -->-derived object to examine.
 * 
 * Tests whether @stream uses a buffered write, and supports the
 * gio_stream_flush() method.
 * 
 * Since: 1.0
 **/

/**
 * GIO_STREAM_IS_SEEKABLE:
 * @stream: the #GioStream<!-- -->-derived object to examine.
 * 
 * Tests whether @stream is seekable, and supports the gio_stream_seek() and
 * gio_stream_tell() methods.
 * 
 * Since: 1.0
 **/

/**
 * GIO_STREAM:
 * @object: the pointer to cast.
 * 
 * Casts the #GioStream derived pointer at @object into a (GioStream*) pointer.
 * Depending on the current debugging level, this function may invoke certain
 * runtime checks to identify invalid casts.
 * 
 * Since: 1.0
 **/

/**
 * GIO_IS_STREAM:
 * @object: the pointer to check.
 * 
 * Checks whether @object is a #GioStream (or derived) instance.
 * 
 * Since: 1.0
 **/

/**
 * GIO_STREAM_CLASS:
 * @klass: the pointer to cast.
 * 
 * Casts a the #GioStreamClass derieved pointer at @klass into a
 * (GioStreamClass*) pointer.
 * 
 * Since: 1.0
 **/

/**
 * GIO_IS_STREAM_CLASS:
 * @klass: the pointer to check.
 * 
 * Checks whether @klass is a #GioStreamClass (or derived) class.
 * 
 * Since: 1.0
 **/

/**
 * GIO_STREAM_GET_CLASS:
 * @object: the pointer to check.
 * 
 * Retrieves a pointer to the #GioStreamClass for the #GioStream (or derived)
 * instance, @object.
 * 
 * Since: 1.0
 **/

/**
 * GIO_TYPE_STREAM:
 * 
 * The registered #GType of the #GioStream class.
 * 
 * Since: 1.0
 **/

/**
 * GIO_TYPE_STREAM_STATE:
 * 
 * The registered #GType of the #GioStreamState enumeration.
 * 
 * Since: 1.0
 **/

/**
 * GIO_TYPE_STREAM_FLAGS:
 * 
 * The registered #GType of the #GioStreamFlags bitwise enumeration.
 * 
 * Since: 1.0
 **/

/**
 * GIO_STREAM_ERROR:
 * 
 * The #GQuark error domain of the #GioStreamError enumeration.
 * 
 * Since: 1.0
 **/

/**
 * GIO_TYPE_STREAM_ERROR:
 * 
 * The registered #GType of the #GioStreamError enumeration.
 * 
 * Since: 1.0
 **/


/* **************** *
 *  Private Macros  *
 * **************** */

#define GIO_STREAM_GET_PRIVATE(object)	(GIO_STREAM (object)->priv)


/* ********************** *
 *  Private Enumerations  *
 * ********************** */

enum
{
  PROP_0,
  PROP_PARENT,
  PROP_STATE,
  PROP_SIZE,
  PROP_FLAGS,
  PROP_EVENT_MASK
};

enum
{
  PARENT_CHANGED,
  EVENTS_CHANGED,
  EVENT,
  STATE_EVENT,
  READABLE_EVENT,
  WRITABLE_EVENT,
  ERROR_EVENT,
  EOF_EVENT,
  OPENED_EVENT,
  CLOSED_EVENT,
  LAST_SIGNAL
};


/* ******************** *
 *  Private Structures  *
 * ******************** */

struct _GioStreamPrivate
{
  GioStream *parent;
  gint state;

  guint freeze_count;
  guint8 event_mask : 5;
};


/* ********************* *
 *  Function Prototypes  *
 * ********************* */

static void            gio_stream_set_property        (GObject       *object,
						       guint          param_id,
						       const GValue  *value,
						       GParamSpec    *pspec);
static void            gio_stream_get_property        (GObject       *object,
						       guint          param_id,
						       GValue        *value,
						       GParamSpec    *pspec);
static void            gio_stream_finalize            (GObject       *object);

/* Default Methods */
static GioStreamFlags  gio_stream_default_get_flags   (GioStream     *stream);
static gssize          gio_stream_default_get_size    (GioStream     *stream);
static gboolean        gio_stream_default_open        (GioStream     *stream,
						       GioError     **error);
static gboolean        gio_stream_default_close       (GioStream     *stream,
						       GioError     **error);

/* Default Signal Handlers */
static gboolean        gio_stream_default_event       (GioStream     *stream,
						       GioEvent      *event);
static void            gio_stream_default_state_event (GioStream     *stream,
						       GioEventState *event);


/* ****************** *
 *  Global Variables  *
 * ****************** */

static guint gio_stream_signals[LAST_SIGNAL] = { 0 };


/* ******************* *
 *  GType Declaration  *
 * ******************* */

G_DEFINE_ABSTRACT_TYPE (GioStream, gio_stream, GIO_TYPE_OBJECT);


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gio_stream_class_init (GioStreamClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);

  gobject_class->set_property = gio_stream_set_property;
  gobject_class->get_property = gio_stream_get_property;
  gobject_class->finalize = gio_stream_finalize;

  class->get_flags = gio_stream_default_get_flags;
  class->get_size = gio_stream_default_get_size;
  class->open = gio_stream_default_open;
  class->close = gio_stream_default_close;
  class->read = NULL;
  class->peek = NULL;
  class->write = NULL;
  class->flush = NULL;
  class->seek = NULL;
  class->tell = NULL;

  class->parent_changed = NULL;
  class->events_changed = NULL;
  class->event = gio_stream_default_event;
  class->state_event = gio_stream_default_state_event;
  class->readable_event = NULL;
  class->writable_event = NULL;
  class->eof_event = NULL;
  class->error_event = NULL;
  class->opened_event = NULL;
  class->closed_event = NULL;

/**
 * GioStream::parent-changed:
 * @stream: the object which emitted this signal.
 * @old_parent: the former parent of the stream.
 * 
 * This signal is emitted by objects after the parent stream object has been
 * changed with gio_stream_set_parent() or g_object_set().
 * 
 * Since: 1.0
 **/
  gio_stream_signals[PARENT_CHANGED] =
    g_signal_new ("parent-changed",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GioStreamClass, parent_changed),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__OBJECT,
		  G_TYPE_NONE, 1,
		  GIO_TYPE_STREAM);

/**
 * GioStream::events-changed:
 * @stream: the object which emitted this signal.
 * @old_events: the former parent of the stream.
 * 
 * This signal is emitted by objects after the desired event mask has been
 * changed with gio_stream_set_event_mask() or g_object_set().
 * 
 * Since: 1.0
 **/
  gio_stream_signals[EVENTS_CHANGED] =
    g_signal_new ("events-changed",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GioStreamClass, events_changed),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__FLAGS,
		  G_TYPE_NONE, 1,
		  GIO_TYPE_EVENT_TYPE);

/**
 * GioStream::event:
 * @stream: the object which emitted this signal.
 * @event: the event which must be processed.
 * 
 * This signal is emitted by objects when an IO event (readable, writable,
 * error, or eof) has occured.
 * 
 * Returns: %TRUE if the event was handled, %FALSE if it was not.
 * 
 * Since: 1.0
 **/
  gio_stream_signals[EVENT] =
    g_signal_new ("event",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GioStreamClass, event),
		  g_signal_accumulator_true_handled, NULL,
		  _gio_marshal_BOOLEAN__BOXED,
		  G_TYPE_BOOLEAN, 1,
		  GIO_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);

/**
 * GioStream::state:
 * @stream: the object which emitted this signal.
 * @event: the state event which must be processed.
 * 
 * This signal is emitted by objects when an IO event (readable, writable,
 * error, eof, or state) has occured.
 * 
 * Returns: %TRUE if a the event was handled, %FALSE if it was not.
 * 
 * Since: 1.0
 **/
  gio_stream_signals[STATE_EVENT] =
    g_signal_new ("state-event",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GioStreamClass, state_event),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__BOXED,
		  G_TYPE_NONE, 1,
		  GIO_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);

/**
 * GioStream::readable:
 * @stream: the object which emitted this signal.
 * 
 * This signal is emitted by objects when data is waiting to be read. Connected
 * handlers which successfully call gio_stream_read() should return %TRUE, which
 * will stop the signal emission. Handlers which do not call gio_stream_read()
 * should return %FALSE.
 * 
 * Returns: %TRUE if a successful read was performed, %FALSE if one was not.
 * 
 * Since: 1.0
 **/
  gio_stream_signals[READABLE_EVENT] =
    g_signal_new ("readable-event",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GioStreamClass, readable_event),
		  g_signal_accumulator_true_handled, NULL,
		  _gio_marshal_BOOLEAN__BOXED,
		  G_TYPE_BOOLEAN, 1,
		  GIO_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);

/**
 * GioStream::writable:
 * @stream: the object which emitted this signal.
 * @event: the event structure.
 * 
 * This signal is emitted by objects when data can be written without blocking.
 * Connected handlers which successfully call gio_stream_write() should return
 * %TRUE, which will stop the signal emission. Handlers which do not call
 * gio_stream_write() should return %FALSE.
 * 
 * Returns: %TRUE if a successful write was performed, %FALSE if one was not.
 * 
 * Since: 1.0
 **/
  gio_stream_signals[WRITABLE_EVENT] =
    g_signal_new ("writable-event",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GioStreamClass, writable_event),
		  g_signal_accumulator_true_handled, NULL,
		  _gio_marshal_BOOLEAN__BOXED,
		  G_TYPE_BOOLEAN, 1,
		  GIO_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);

/**
 * GioStream::error:
 * @stream: the object which emitted this signal.
 * @event: the error event.
 * 
 * This signal is emitted by objects when an IO error has occured while
 * performing an asynchronous read or write.
 * 
 * Returns: %TRUE if a the event was handled, %FALSE if it was not.
 * 
 * Since: 1.0
 **/
  gio_stream_signals[ERROR_EVENT] =
    g_signal_new ("error-event",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GioStreamClass, error_event),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__BOXED,
		  G_TYPE_NONE, 1,
		  GIO_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);

/**
 * GioStream::eof:
 * @stream: the object which emitted this signal.
 * @event: the EOF event.
 * 
 * This signal is emitted by objects when an end-of-file event has occurred and
 * the stream should be closed.
 * 
 * Returns: %TRUE if a the event was handled, %FALSE if it was not.
 * 
 * Since: 1.0
 **/
  gio_stream_signals[EOF_EVENT] =
    g_signal_new ("eof-event",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GioStreamClass, eof_event),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__BOXED,
		  G_TYPE_NONE, 1,
		  GIO_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);

/**
 * GioStream::opened:
 * @stream: the object which emitted this signal.
 * @event: the state event which must be processed.
 * 
 * This signal is emitted by objects when an "opened" state-event has occured.
 * 
 * Returns: %TRUE if a the event was handled, %FALSE if it was not.
 * 
 * Since: 1.0
 **/
  gio_stream_signals[OPENED_EVENT] =
    g_signal_new ("opened-event",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GioStreamClass, opened_event),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__BOXED,
		  G_TYPE_NONE, 1,
		  GIO_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);

/**
 * GioStream::closed:
 * @stream: the object which emitted this signal.
 * @event: the state event which must be processed.
 * 
 * This (convenience) signal is emitted by objects when an "closed" state-event
 * has occured.
 * 
 * Returns: %TRUE if a the event was handled, %FALSE if it was not.
 * 
 * Since: 1.0
 **/
  gio_stream_signals[CLOSED_EVENT] =
    g_signal_new ("closed-event",
		  G_TYPE_FROM_CLASS (class),
		  G_SIGNAL_RUN_LAST,
		  G_STRUCT_OFFSET (GioStreamClass, closed_event),
		  NULL, NULL,
		  g_cclosure_marshal_VOID__BOXED,
		  G_TYPE_NONE, 1,
		  GIO_TYPE_EVENT | G_SIGNAL_TYPE_STATIC_SCOPE);

  g_object_class_install_property (gobject_class, PROP_PARENT,
				   g_param_spec_object ("parent",
							_("Parent"),
							_("The parent stream of this object, that writes are sent to and reads are pulled from."),
							GIO_TYPE_STREAM,
							G_PARAM_READWRITE));
  g_object_class_install_property (gobject_class, PROP_STATE,
				   g_param_spec_int ("state",
						     _("State"),
						     _("The current state of this stream."),
						     G_MININT,
						     G_MAXINT,
						     GIO_STREAM_STATE_CLOSED,
						     G_PARAM_READABLE));

  g_object_class_install_property (gobject_class, PROP_SIZE,
				   g_param_spec_int64 ("size",
						       _("Size"),
						       _("The size of this stream, in bytes."),
						       G_MININT64,
						       G_MAXINT64,
						       -1,
						       G_PARAM_READABLE));
  g_object_class_install_property (gobject_class, PROP_FLAGS,
				   g_param_spec_flags ("flags",
						       _("Flags"),
						       _("This stream's flags."),
						       GIO_TYPE_STREAM_FLAGS,
						       0,
						       G_PARAM_READABLE));
  g_object_class_install_property (gobject_class, PROP_EVENT_MASK,
				   g_param_spec_flags ("event-mask",
						       _("Event Mask"),
						       _("This asynchronous events this stream listens for. If any are set, the stream will be placed in asynchronous mode."),
						       GIO_TYPE_EVENT_TYPE,
						       0,
						       G_PARAM_READWRITE));

  g_type_class_add_private (class, sizeof (GioStreamPrivate));
}

static void
gio_stream_init (GioStream *stream)
{
  stream->priv = G_TYPE_INSTANCE_GET_PRIVATE (stream, GIO_TYPE_STREAM, GioStreamPrivate);
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gio_stream_set_property (GObject      *object,
			 guint         param_id,
			 const GValue *value,
			 GParamSpec   *pspec)
{
  GioStream *stream = GIO_STREAM (object);

  switch (param_id)
    {
    case PROP_PARENT:
      gio_stream_set_parent (stream, g_value_get_object (value));
      break;
    case PROP_EVENT_MASK:
      gio_stream_set_event_mask (stream, g_value_get_flags (value));
      break;
  
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
      break;
    }
}

static void
gio_stream_get_property (GObject    *object,
			 guint       param_id,
			 GValue     *value,
			 GParamSpec *pspec)
{
  GioStreamPrivate *priv = GIO_STREAM_GET_PRIVATE (object);

  switch (param_id)
    {
    case PROP_PARENT:
      g_value_set_object (value, priv->parent);
      break;
    case PROP_STATE:
      g_value_set_int (value, priv->state);
      break;
    case PROP_SIZE:
      g_value_set_int64 (value, gio_stream_get_size (GIO_STREAM (object)));
      break;
    case PROP_FLAGS:
      g_value_set_flags (value, gio_stream_get_flags (GIO_STREAM (object)));
      break;
    case PROP_EVENT_MASK:
      g_value_set_flags (value, priv->event_mask);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
      break;
    }
}

static void
gio_stream_finalize (GObject *object)
{
  GioStreamPrivate *priv = GIO_STREAM_GET_PRIVATE (object);

  if (priv->parent)
    g_object_unref (priv->parent);

  if (G_OBJECT_CLASS (gio_stream_parent_class)->finalize != NULL)
    (*G_OBJECT_CLASS (gio_stream_parent_class)->finalize) (object);
}


/* ********************* *
 *  GioStream Functions  *
 * ********************* */

/* Default Methods */
static GioStreamFlags
gio_stream_default_get_flags (GioStream *stream)
{
  return 0;
}

static gssize
gio_stream_default_get_size (GioStream *stream)
{
  return -1;
}

static gboolean
gio_stream_default_open (GioStream  *stream,
                         GioError  **error)
{
  if (stream->priv->parent)
    return gio_stream_open (stream->priv->parent, error);

  return FALSE;
}

static gboolean
gio_stream_default_close (GioStream  *stream,
			  GioError  **error)
{
  if (stream->priv->parent)
    return gio_stream_close (stream->priv->parent, error);

  return TRUE;
}

/* Default Signal Handlers */
static gboolean
gio_stream_default_event (GioStream *stream,
			  GioEvent  *event)
{
  gboolean retval;

  switch (event->type)
    {
    case GIO_EVENT_STATE:
      g_signal_emit (stream, gio_stream_signals[STATE_EVENT], 0, event, &retval);
      stream->priv->state = event->state.state;
      break;
    case GIO_EVENT_READABLE:
      g_signal_emit (stream, gio_stream_signals[READABLE_EVENT], 0, event, &retval);
      break;
    case GIO_EVENT_WRITABLE:
      g_signal_emit (stream, gio_stream_signals[WRITABLE_EVENT], 0, event, &retval);
      break;
    case GIO_EVENT_EOF:
      g_signal_emit (stream, gio_stream_signals[EOF_EVENT], 0, event, &retval);
      break;
    case GIO_EVENT_ERROR:
      g_signal_emit (stream, gio_stream_signals[ERROR_EVENT], 0, event, &retval);
      break;
    default:
      g_assert_not_reached ();
      retval = TRUE;
      break;
    }

  return retval;
}

static void
gio_stream_default_state_event (GioStream     *stream,
				GioEventState *event)
{
  switch (event->state)
    {
    case GIO_STREAM_STATE_OPEN:
      g_signal_emit (stream, gio_stream_signals[OPENED_EVENT], 0, event);
      break;
    case GIO_STREAM_STATE_CLOSED:
      g_signal_emit (stream, gio_stream_signals[CLOSED_EVENT], 0, event);
      break;
    default:
      break;
    }
}


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static gboolean
get_stream_module_and_type (const gchar  *name,
			    GTypeModule **module,
			    GType        *type)
{
  *module = (GTypeModule *) gio_module_get_for_type (name);

  if (!(*module))
    {
      g_critical ("No modules containing streams of type `%s' could be found.",
		  name);
      return FALSE;
    }

  *type = g_type_from_name (name);
  if (!g_type_is_a (*type, GIO_TYPE_STREAM))
    {
      g_critical ("The type `%s' is not a subclass of `%s'.", name,
		  g_type_name (GIO_TYPE_STREAM));
      g_type_module_unuse (G_TYPE_MODULE (*module));
      return FALSE;
    }

  return TRUE;
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

/**
 * gio_stream_new:
 * @stream_type_name: the name of the type.
 * @first_property_name: the first property name to set, or %NULL.
 * @Varargs: A %NULL-terminated list of property names and values.
 * 
 * Creates a new GioStream object, of type @stream_type_name, setting the
 * properties listed at construct-time.
 * 
 * <informalexample>
 *   stream = gio_stream_new ("GioFileStream",
 *                            "filename", "/etc/hosts",
 *                            NULL);
 * </informalexample>
 * 
 * Returns: a newly allocated GioStream object, which must be unreferenced with
 *  g_object_unref() when no longer needed.
 * 
 * Since: 1.0
 **/
GioStream *
gio_stream_new (const gchar *stream_type_name,
		const gchar *first_property_name,
		...)
{
  GTypeModule *module;
  GType stream_type;
  va_list var_args;
  GioStream *retval;

  if (!get_stream_module_and_type (stream_type_name, &module, &stream_type))
    return NULL;

  va_start (var_args, first_property_name);
  retval = (GioStream *) g_object_new_valist (stream_type, first_property_name,
					      var_args);
  va_end (var_args);

  g_type_module_unuse (module);

  return retval;
}

/**
 * gio_stream_new_valist:
 * @stream_type_name:
 * @first_property_name:
 * @var_args: 
 * 
 * 
 * 
 * Returns: a newly allocated GioStream object, which must be unreferenced with
 *  g_object_unref() when no longer needed.
 * 
 * Since: 1.0
 **/
GioStream *
gio_stream_new_valist (const gchar *stream_type_name,
		       const gchar *first_property_name,
		       va_list      var_args)
{
  GTypeModule *module;
  GType stream_type;
  GioStream *retval;

  if (!get_stream_module_and_type (stream_type_name, &module, &stream_type))
    return NULL;

  retval = (GioStream *) g_object_new_valist (stream_type, first_property_name,
					      var_args);
  g_type_module_unuse (module);

  return retval;
}

/**
 * gio_stream_newv:
 * @stream_type_name: 
 * @first_property_name:
 * @params: 
 * 
 * 
 * 
 * Returns: a newly allocated GioStream object, which must be unreferenced with
 *  g_object_unref() when no longer needed.
 * 
 * Since: 1.0
 **/
GioStream *
gio_stream_newv (const gchar *stream_type_name,
		 guint        n_params,
		 GParameter  *params)
{
  GTypeModule *module;
  GType stream_type;
  GioStream *retval;

  if (!get_stream_module_and_type (stream_type_name, &module, &stream_type))
    return NULL;

  retval = g_object_newv (stream_type, n_params, params);
  g_type_module_unuse (module);

  return retval;
}

/**
 * gio_stream_get_flags:
 * @stream: the stream object to examine.
 * 
 * Retrieves the bitwise flags for the capabilities of @stream.
 * 
 * Returns: the capabilities flags of @stream.
 * 
 * Since: 1.0
 **/
GioStreamFlags
gio_stream_get_flags (GioStream *stream)
{
  GioStreamClass *class;
  
  g_return_val_if_fail (GIO_IS_STREAM (stream), 0);

  class = GIO_STREAM_GET_CLASS (stream);

  g_return_val_if_fail (class->get_flags != NULL, 0);
  
  return (*class->get_flags) (stream);
}

/**
 * gio_stream_get_size:
 * @stream: the stream object to examine.
 * 
 * Retrieves the size (in bytes) of the data accessed by @stream. If a stream
 * does not know it's size (such as a socket or a parser), then %-1 will be
 * returned.
 * 
 * Returns: the size of @stream, or %-1.
 * 
 * Since: 1.0
 **/
gssize
gio_stream_get_size (GioStream *stream)
{
  GioStreamClass *class;

  g_return_val_if_fail (GIO_IS_STREAM (stream), -1);

  class = GIO_STREAM_GET_CLASS (stream);

  g_return_val_if_fail (class->get_size != NULL, -1);

  return (*class->get_size) (stream);
}

/**
 * gio_stream_freeze:
 * @stream: the stream object to modify.
 * 
 * Increases the freeze count for @stream by one. When a stream is frozen, it
 * will not emit readable/writable signals, and may safely be modified. The
 * @stream must be thawed the same number of times it has been frozen to
 * actually resume. See also: gio_stream_thaw().
 * 
 * Since: 1.0.
 **/
void
gio_stream_freeze (GioStream *stream)
{
  g_return_if_fail (GIO_IS_STREAM (stream));

  stream->priv->freeze_count++;

  if (stream->priv->freeze_count == 1)
    {
      GioStreamClass *class;

      class = GIO_STREAM_GET_CLASS (stream);

      g_return_if_fail (class->freeze != NULL);

      (*class->freeze) (stream);
    }
}

/**
 * gio_stream_thaw:
 * @stream: the stream object to modify.
 * 
 * Decreases the freeze count for @stream by one. When a stream has been thawed,
 * it will resume emitting readable/writable signals where it left off. The
 * @stream must be thawed the same number of times it has been frozen to
 * actually resume normal operation. See also: gio_stream_freeze().
 * 
 * Since: 1.0.
 */
void
gio_stream_thaw (GioStream *stream)
{
  g_return_if_fail (GIO_IS_STREAM (stream));

  stream->priv->freeze_count--;

  if (stream->priv->freeze_count == 0)
    {
      GioStreamClass *class;

      class = GIO_STREAM_GET_CLASS (stream);

      g_return_if_fail (class->thaw != NULL);

      (*class->thaw) (stream);
    }
}

/**
 * gio_stream_open:
 * @stream: the stream to open.
 * @error: the location of a pointer to store errors which occurred, or %NULL.
 *
 * Opens the @stream in question. This function will wait until @stream is
 * opened, or an error occurred, before returning. If the stream was
 * successfully opened, %TRUE will be returned, otherwise, %FALSE will be
 * returned and @error will be set.
 *
 * <note>While this function is running, the thread it is running in will stop
 * executing. This means that when this function is run from inside the main
 * application thread, the application will appear to freeze. If this is not
 * acceptable, use gio_stream_start_open() instead, and connect to the
 * "state-changed" signal to determine when the open process has
 * finished.</note>
 * 
 * Returns: %TRUE if @stream is currently open, %FALSE if not.
 *
 * Since: 1.0
 **/
gboolean
gio_stream_open (GioStream  *stream,
		 GioError  **error)
{
  GioStreamClass *class;

  g_return_val_if_fail (GIO_IS_STREAM (stream), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  class = GIO_STREAM_GET_CLASS (stream);

  if (stream->priv->state == GIO_STREAM_STATE_OPEN)
    return TRUE;

  g_return_val_if_fail (class->open != NULL, FALSE);

  return (*class->open) (stream, error);
}

/**
 * gio_stream_close:
 * @stream: the stream to close.
 * @error: the location of a pointer to store errors which occurred, or %NULL.
 *
 * Closes the @stream in question. This function will wait until the stream has
 * finished closing to return. If @stream has been successfully closed, %TRUE
 * will be returned, otherwise %FALSE will be returned and @error will be set.
 *
 * <note>While this function is running, the thread it is running in will stop
 * executing. This means that when this function is run from inside the main
 * application thread, the application will appear to freeze. If this is not
 * acceptable, use gio_stream_start_close() instead, and connect to the
 * "state-changed" signal to determine when the closing process has
 * finished.</note>
 * 
 * Returns: %TRUE on success, %FALSE on failure.
 *
 * Since: 1.0
 **/
gboolean
gio_stream_close (GioStream  *stream,
		  GioError  **error)
{
  GioStreamClass *class;

  g_return_val_if_fail (GIO_IS_STREAM (stream), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);
  g_return_val_if_fail (stream->priv->state > GIO_STREAM_STATE_CLOSED, FALSE);

  class = GIO_STREAM_GET_CLASS (stream);

  g_return_val_if_fail (class->close != NULL, FALSE);

  return (*class->close) (stream, error);
}

/**
 * gio_stream_read:
 * @stream: the stream to read from.
 * @buffer: a pointer to a location to store a returned buffer in.
 * @read_size: the maximum number of bytes to read.
 * @error: the location of a pointer to store a returned error, or %NULL.
 * 
 * Reads up to @read_size bytes from @stream, and places them in a new @buffer
 * object. If an error occurs, @error will be set and %FALSE will be returned,
 * otherwise %TRUE will be returned. Note that @stream must be readable, which
 * can be determined using GIO_STREAM_IS_READABLE().
 * 
 * Returns: %TRUE if the read succeeded, %FALSE if it failed.
 * 
 * Since: 1.0
 **/
gboolean
gio_stream_read (GioStream  *stream,
		 GioBuffer **buffer,
		 gssize      read_size,
		 GioError  **error)
{
  GioStreamClass *class;

  g_return_val_if_fail (GIO_IS_STREAM (stream), FALSE);
  g_return_val_if_fail (buffer != NULL && *buffer == NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);
  g_return_val_if_fail (GIO_STREAM_IS_READABLE (stream), FALSE);
  g_return_val_if_fail (gio_stream_get_state (stream) == GIO_STREAM_STATE_OPEN,
			FALSE);

  class = GIO_STREAM_GET_CLASS (stream);

  g_return_val_if_fail (class->read != NULL, FALSE);

  return (*class->read) (stream, buffer, read_size, error);
}

/**
 * gio_stream_peek:
 * @stream: the stream to examine.
 * @error: a pointer to a location to store errors in.
 * 
 * Retrieves a reference to the buffer used by @stream. If there was an error
 * accessing the buffer, %NULL will be returned, and @error will be set. Note
 * that @stream must be read-buffered, which can be determined using
 * GIO_STREAM_IS_READ_BUFFERED().
 * 
 * Returns: a reference to a #GioBuffer object, which must be released with
 *  g_object_unref() when no longer needed, or %NULL.
 * 
 * Since: 1.0
 **/
GioBuffer *
gio_stream_peek (GioStream *stream)
{
  GioStreamClass *class;
  gint state;

  g_return_val_if_fail (GIO_IS_STREAM (stream), NULL);
  g_return_val_if_fail (GIO_STREAM_IS_READ_BUFFERED (stream), NULL);

  state = gio_stream_get_state (stream);
  g_return_val_if_fail (state == GIO_STREAM_STATE_OPEN ||
			state == GIO_STREAM_STATE_FROZEN, NULL);

  class = GIO_STREAM_GET_CLASS (stream);
  g_return_val_if_fail (class->peek != NULL, NULL);

  return (*class->peek) (stream);
}

/**
 * gio_stream_write:
 * @stream: the stream object to write to.
 * @buffer: the data to write.
 * @bytes_written: a pointer to a location to store the number of bytes from @buffer actually written.
 * @error: a pointer to a location to store an error which occurred, or %NULL.
 * 
 * Attempts to write the contents of @buffer to @stream. If some of the data
 * could be written, @bytes_written will be set to the number of bytes actually
 * written, and %TRUE will be returned. If an error occured, %FALSE will be
 * returned and @error will be set.
 * 
 * Returns: %TRUE on success, %FALSE on error.
 * 
 * Since: 1.0
 **/
gboolean
gio_stream_write (GioStream  *stream,
		  GioBuffer  *buffer,
		  gsize      *bytes_written,
		  GioError  **error)
{
  GioStreamClass *class;

  g_return_val_if_fail (GIO_IS_STREAM (stream), FALSE);
  g_return_val_if_fail (buffer != NULL, FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);
  g_return_val_if_fail (GIO_STREAM_IS_WRITABLE (stream), FALSE);
  g_return_val_if_fail (gio_stream_get_state (stream) == GIO_STREAM_STATE_OPEN, FALSE);

  class = GIO_STREAM_GET_CLASS (stream);

  g_return_val_if_fail (class->write != NULL, FALSE);

  return (*class->write) (stream, buffer, bytes_written, error);
}

/**
 * gio_stream_flush:
 * @stream: the buffered stream to flush.
 * 
 * Flushes any pending writes from @stream. Note that @stream should be a
 * write-buffered stream, which can be tested with
 * GIO_STREAM_IS_WRITE_BUFFERED().
 * 
 * Since: 1.0
 **/
void
gio_stream_flush (GioStream *stream)
{
  GioStreamClass *class;

  g_return_if_fail (GIO_IS_STREAM (stream));
  g_return_if_fail (GIO_STREAM_IS_WRITE_BUFFERED (stream));
  g_return_if_fail (gio_stream_get_state (stream) == GIO_STREAM_STATE_OPEN);

  class = GIO_STREAM_GET_CLASS (stream);

  g_return_if_fail (class->flush != NULL);

  (*class->flush) (stream);
}

/**
 * gio_stream_seek:
 * @stream: the seekable stream to use.
 * @offset: the offset to seek to.
 * @whence: the location which offset applies to.
 * @error: a pointer to a location to store errors in, or %NULL.
 * 
 * Changes the position of the data used by @stream for by adding @offset to
 * @whence. If successful, the new offset from the start of the stream will be
 * returned. If an error occurs, @error will be set and %-1 will be returned.
 * 
 * Returns: the current offset from the start of the stream, or %-1 on error.
 * 
 * Since: 1.0
 **/
gssize
gio_stream_seek (GioStream  *stream,
		 gssize      offset,
		 GSeekType   whence,
		 GioError  **error)
{
  GioStreamClass *class;

  g_return_val_if_fail (GIO_IS_STREAM (stream), -1);
  g_return_val_if_fail (error == NULL || *error == NULL, -1);
  g_return_val_if_fail (GIO_STREAM_IS_SEEKABLE (stream), -1);
  g_return_val_if_fail (gio_stream_get_state (stream) == GIO_STREAM_STATE_OPEN,
			-1);

  class = GIO_STREAM_GET_CLASS (stream);

  g_return_val_if_fail (class->seek != NULL, -1);

  return (*class->seek) (stream, offset, whence, error);
}

/**
 * gio_stream_tell:
 * @stream: the seekable stream to examine.
 * 
 * Retrieves the current offset (in bytes) from the start of the data for
 * @stream, as set by gio_stream_seek() or by successful read/write calls.
 * 
 * Returns: the offset in bytes from the start of @stream.
 * 
 * Since: 1.0
 **/
gssize
gio_stream_tell (GioStream  *stream)
{
  GioStreamClass *class;

  g_return_val_if_fail (GIO_IS_STREAM (stream), -1);
  g_return_val_if_fail (GIO_STREAM_IS_SEEKABLE (stream), -1);
  g_return_val_if_fail (gio_stream_get_state (stream) == GIO_STREAM_STATE_OPEN, -1);

  class = GIO_STREAM_GET_CLASS (stream);

  g_return_val_if_fail (class->tell != NULL, -1);

  return (*class->tell) (stream);
}

/**
 * gio_stream_get_state:
 * @stream: the stream object to examine.
 * 
 * Retrieves the current state of @stream.
 * 
 * Returns: an integer corresponding to the current state of @stream.
 * 
 * Since: 1.0
 **/
gint
gio_stream_get_state (GioStream *stream)
{
  g_return_val_if_fail (GIO_IS_STREAM (stream), GIO_STREAM_STATE_CLOSED);

  return stream->priv->state;
}

/**
 * gio_stream_get_parent:
 * @stream: the stream to examine.
 * 
 * Retrieves a pointer to the parent object of @stream.
 * 
 * Returns: a pointer to the parent of @stream.
 * 
 * Since: 1.0
 **/
GioStream *
gio_stream_get_parent (GioStream *stream)
{
  g_return_val_if_fail (GIO_IS_STREAM (stream), NULL);
  g_return_val_if_fail (!GIO_STREAM_IS_TOPLEVEL (stream), NULL);

  return stream->priv->parent;
}

/**
 * gio_stream_set_parent:
 * @stream: the stream to modify.
 * @parent: the new parent object.
 * 
 * Sets the parent object of @stream to @parent, and emits the "parent-changed"
 * signal.
 * 
 * Since: 1.0
 **/
void
gio_stream_set_parent (GioStream *stream,
		       GioStream *parent)
{
  GioStream *old_parent;

  g_return_if_fail (GIO_IS_STREAM (stream));
  g_return_if_fail (parent == NULL || GIO_IS_STREAM (parent));
  
  old_parent = stream->priv->parent;
  if (parent)
    stream->priv->parent = g_object_ref (parent);
  else
    stream->priv->parent = NULL;
  g_signal_emit (stream, gio_stream_signals[PARENT_CHANGED], 0, old_parent);
  g_object_notify (G_OBJECT (stream), "parent");

  if (old_parent)
    g_object_unref (old_parent);
}

GioEventType
gio_stream_get_event_mask (GioStream *stream)
{
  g_return_val_if_fail (GIO_IS_STREAM (stream), GIO_EVENT_NONE);

  return stream->priv->event_mask;
}

void
gio_stream_set_event_mask (GioStream    *stream,
			   GioEventType  events)
{
  GioEventType old_mask;

  g_return_if_fail (GIO_IS_STREAM (stream));
  g_return_if_fail (events >= GIO_EVENT_NONE && events <= GIO_EVENT_ERROR);

  old_mask = stream->priv->event_mask;
  stream->priv->event_mask = events;
  g_signal_emit (stream, gio_stream_signals[EVENTS_CHANGED], 0, old_mask);
}

static gboolean
do_event (GioStream *stream,
	  GioEvent  *event)
{
  gboolean retval;

  g_object_ref (stream);
  g_signal_emit (stream, gio_stream_signals[EVENT], 0, event, &retval);
  g_object_unref (stream);

  return retval;
}

gboolean
gio_stream_event (GioStream *stream,
		  GioEvent  *event)
{
  g_return_val_if_fail (GIO_IS_STREAM (stream), FALSE);
  g_return_val_if_fail (GIO_IS_EVENT (event), FALSE);

  return do_event (stream, event);
}

gboolean 
gio_stream_readable_event (GioStream *stream)
{
  gboolean retval;
  GioEvent *event;

  g_return_val_if_fail (GIO_IS_STREAM (stream), FALSE);

  event = gio_event_new (GIO_EVENT_READABLE);
  retval = do_event (stream, event);
  gio_event_free (event);

  return retval;
}

gboolean 
gio_stream_writable_event (GioStream *stream)
{
  gboolean retval;
  GioEvent *event;

  g_return_val_if_fail (GIO_IS_STREAM (stream), FALSE);

  event = gio_event_new (GIO_EVENT_WRITABLE);
  retval = do_event (stream, event);
  gio_event_free (event);

  return retval;
}

void
gio_stream_state_event (GioStream *stream,
			gint       new_state)
{
  GioEvent *event;

  g_return_if_fail (GIO_IS_STREAM (stream));

  event = gio_event_new (GIO_EVENT_STATE);
  event->state.state = new_state;
  do_event (stream, event);
  gio_event_free (event);
}

void 
gio_stream_error_event (GioStream *stream,
			GSList    *error_list)
{
  GioEvent *event;

  g_return_if_fail (GIO_IS_STREAM (stream));

  event = gio_event_new (GIO_EVENT_ERROR);
  event->error.error_list = gio_error_list_copy (error_list);
  do_event (stream, event);
  gio_event_free (event);
}

void 
gio_stream_error_event_single (GioStream *stream,
			       GioError  *error)
{
  GioEvent *event;

  g_return_if_fail (GIO_IS_STREAM (stream));

  event = gio_event_new (GIO_EVENT_ERROR);
  event->error.error_list = g_slist_append (event->error.error_list,
					    gio_error_copy (error));
  do_event (stream, event);
  gio_event_free (event);
}

void
gio_stream_eof_event (GioStream *stream)
{
  GioEvent *event;

  g_return_if_fail (GIO_IS_STREAM (stream));

  event = gio_event_new (GIO_EVENT_EOF);
  do_event (stream, event);
  gio_event_free (event);
}

G_LOCK_DEFINE_STATIC (quark);

GQuark
gio_stream_error_get_quark (void)
{
  static volatile GQuark quark = 0;

  G_LOCK (quark);

  if (quark == 0)
    quark = g_quark_from_static_string ("gio-stream-error");

  G_UNLOCK (quark);

  return quark;
}


/**
 * gio_set_stream_error:
 * @error: a pointer to a location to store the error, or %NULL.
 * @code: the stream error code to use.
 * @format: the format string for the error message.
 * @Varargs: the parameters for @format.
 * 
 * Convenience function to set a stream error for an error return.
 * 
 * Since: 1.0
 **/
void
gio_set_stream_error (GioError       **error,
		      GioStreamError   code,
		      const gchar     *format,
		      ...)
{                                                                  
  va_list args;
                                                                                
  g_return_if_fail (error == NULL || *error == NULL);

  if (error == NULL)
    return;

  *error = g_new (GioError, 1);
  (*error)->error.domain = GIO_STREAM_ERROR;
  (*error)->error.code = code;

  va_start (args, format);
  (*error)->error.message = g_strdup_vprintf (format, args);
  va_end (args);
}
