/* 
 * Gio Library: gio/gio-stream.h
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * An abstract base class for stream elements.
 */

#ifndef __GIO_STREAM_H__
#define __GIO_STREAM_H__ 1

#include "gio-buffer.h"
#include "gio-error.h"
#include "gio-event.h"
#include "gio-object.h"

G_BEGIN_DECLS


#define GIO_TYPE_STREAM \
  (gio_stream_get_type ())
#define GIO_STREAM(object) \
  (G_TYPE_CHECK_INSTANCE_CAST ((object), GIO_TYPE_STREAM, GioStream))
#define GIO_IS_STREAM(object) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((object), GIO_TYPE_STREAM))
#define GIO_STREAM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GIO_TYPE_STREAM, GioStreamClass))
#define GIO_IS_STREAM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GIO_TYPE_STREAM))
#define GIO_STREAM_GET_CLASS(object) \
  (G_TYPE_INSTANCE_GET_CLASS ((object), GIO_TYPE_STREAM, GioStreamClass))

#define GIO_STREAM_IS_TOPLEVEL(stream) \
  ((gio_stream_get_flags (GIO_STREAM (stream)) & GIO_STREAM_TOPLEVEL) == GIO_STREAM_TOPLEVEL)

#define GIO_STREAM_IS_READABLE(stream) \
  ((gio_stream_get_flags (GIO_STREAM (stream)) & GIO_STREAM_READABLE) == GIO_STREAM_READABLE)
#define GIO_STREAM_IS_READ_BUFFERED(stream) \
  ((gio_stream_get_flags (GIO_STREAM (stream)) & GIO_STREAM_READ_BUFFERED) == GIO_STREAM_READ_BUFFERED)

#define GIO_STREAM_IS_WRITABLE(stream) \
  ((gio_stream_get_flags (GIO_STREAM (stream)) & GIO_STREAM_WRITABLE) == GIO_STREAM_WRITABLE)
#define GIO_STREAM_IS_WRITE_BUFFERED(stream) \
  ((gio_stream_get_flags (GIO_STREAM (stream)) & GIO_STREAM_WRITE_BUFFERED) == GIO_STREAM_WRITE_BUFFERED)

#define GIO_STREAM_IS_SEEKABLE(stream) \
  ((gio_stream_get_flags (GIO_STREAM (stream)) & GIO_STREAM_SEEKABLE) == GIO_STREAM_SEEKABLE)

#define GIO_STREAM_WANT_READABLE_EVENTS(stream) \
  ((gio_stream_get_event_mask (GIO_STREAM (stream)) & GIO_EVENT_READABLE) == GIO_EVENT_READABLE)
#define GIO_STREAM_WANT_WRITABLE_EVENTS(stream) \
  ((gio_stream_get_event_mask (GIO_STREAM (stream)) & GIO_EVENT_WRITABLE) == GIO_EVENT_WRITABLE)
#define GIO_STREAM_WANT_STATE_EVENTS(stream) \
  ((gio_stream_get_event_mask (GIO_STREAM (stream)) & GIO_EVENT_STATE) == GIO_EVENT_STATE)
#define GIO_STREAM_WANT_ERROR_EVENTS(stream) \
  ((gio_stream_get_event_mask (GIO_STREAM (stream)) & GIO_EVENT_ERROR) == GIO_EVENT_ERROR)
#define GIO_STREAM_WANT_EOF_EVENTS(stream) \
  ((gio_stream_get_event_mask (GIO_STREAM (stream)) & GIO_EVENT_EOF) == GIO_EVENT_EOF)

#define GIO_STREAM_ERROR \
  (gio_stream_error_get_quark ())


typedef enum /* <flags,prefix=GIO_STREAM> */
{
  GIO_STREAM_TOPLEVEL       = 1 << 0,
  GIO_STREAM_READABLE       = 1 << 1,
  GIO_STREAM_READ_BUFFERED  = 1 << 2,
  GIO_STREAM_WRITABLE       = 1 << 3,
  GIO_STREAM_WRITE_BUFFERED = 1 << 4,
  GIO_STREAM_SEEKABLE       = 1 << 5
}
GioStreamFlags;

typedef enum /* <prefix=GIO_STREAM_STATE> */
{
  GIO_STREAM_STATE_FIRST   = -3,

  GIO_STREAM_STATE_CLOSING = -1,
  GIO_STREAM_STATE_CLOSED  =  0,
  GIO_STREAM_STATE_OPENING =  1,
  GIO_STREAM_STATE_LAST    =  2,

  GIO_STREAM_STATE_OPEN    =  G_MAXINT,
  GIO_STREAM_STATE_FROZEN  =  G_MININT
}
GioStreamState;

typedef enum /* <prefix=GIO_STREAM_ERROR> */
{
  GIO_STREAM_ERROR_UNKNOWN
}
GioStreamError;


typedef struct _GioStream GioStream;
typedef struct _GioStreamClass GioStreamClass;
typedef struct _GioStreamPrivate GioStreamPrivate;

typedef GioStreamFlags  (*GioStreamGetFlagsFunc)   (GioStream  *stream);
typedef gssize          (*GioStreamGetSizeFunc)    (GioStream  *stream);

typedef void            (*GioStreamFreezeFunc)     (GioStream  *stream);
typedef void            (*GioStreamThawFunc)       (GioStream  *stream);

typedef gboolean        (*GioStreamStartAsyncFunc) (GioStream  *stream,
						    GioError  **error);
typedef gboolean        (*GioStreamStopAsyncFunc)  (GioStream  *stream,
						    GioError  **error);

typedef gboolean        (*GioStreamOpenFunc)       (GioStream  *stream,
						    GioError  **error);
typedef gboolean        (*GioStreamCloseFunc)      (GioStream  *stream,
						    GioError  **error);

typedef gboolean        (*GioStreamReadFunc)       (GioStream  *stream,
						    GioBuffer **buffer,
						    gssize      read_size,
						    GioError  **error);
typedef GioBuffer      *(*GioStreamPeekFunc)       (GioStream  *stream);

typedef gboolean        (*GioStreamWriteFunc)      (GioStream  *stream,
						    GioBuffer  *buffer,
						    gsize      *bytes_written,
						    GioError  **error);
typedef void            (*GioStreamFlushFunc)      (GioStream  *stream);

typedef gssize          (*GioStreamSeekFunc)       (GioStream  *stream,
						    gssize      offset,
						    GSeekType   whence,
						    GioError  **error);
typedef gssize          (*GioStreamTellFunc)       (GioStream  *stream);


struct _GioStream
{
  /* <private> */
  GioObject parent;

  GioStreamPrivate *priv;
};

struct _GioStreamClass
{
  /* <private> */
  GioObjectClass parent_class;

  /* <public> */
  /* Methods */
  GioStreamFlags   (*get_flags)      (GioStream        *stream);
  gssize           (*get_size)       (GioStream        *stream);

  void             (*freeze)         (GioStream        *stream);
  void             (*thaw)           (GioStream        *stream);

  gboolean         (*open)           (GioStream        *stream,
				      GioError        **error);
  gboolean         (*close)          (GioStream        *stream,
				      GioError        **error);

  gboolean         (*read)           (GioStream        *stream,
				      GioBuffer       **buffer,
				      gssize            read_size,
				      GioError        **error);
  GioBuffer       *(*peek)           (GioStream        *stream);

  gboolean         (*write)          (GioStream        *stream,
				      GioBuffer        *buffer,
				      gsize            *bytes_written,
				      GioError        **error);
  void             (*flush)          (GioStream        *stream);

  gssize           (*seek)           (GioStream        *stream,
				      gssize            offset,
				      GSeekType         whence,
				      GioError        **error);
  gssize           (*tell)           (GioStream        *stream);

  /* <private> */
  void (*__gio_reserved1);
  void (*__gio_reserved2);
  void (*__gio_reserved3);
  void (*__gio_reserved4);

  /* <public> */
  /* Signals */
  void            (*parent_changed) (GioStream        *stream,
				     GioStream        *old_parent);
  void            (*events_changed) (GioStream        *stream,
				     GioEventType      old_events);

  gboolean        (*event)          (GioStream        *stream,
				     GioEvent         *event);
  gboolean        (*readable_event) (GioStream        *stream,
				     GioEventReadable *event);
  gboolean        (*writable_event) (GioStream        *stream,
				     GioEventWritable *event);
  void            (*error_event)    (GioStream        *stream,
				     GioEventError    *event);
  void            (*eof_event)      (GioStream        *stream,
				     GioEventEof      *event);
  void            (*state_event)    (GioStream        *stream,
				     GioEventState    *event);
  void            (*opened_event)   (GioStream        *stream,
				     GioEventState    *event);
  void            (*closed_event)   (GioStream        *stream,
				     GioEventState    *event);

  /* <private> */
  void (*__gio_reserved5);
  void (*__gio_reserved6);
  void (*__gio_reserved7);
  void (*__gio_reserved8);
};


GType           gio_stream_get_type           (void) G_GNUC_CONST;

GioStream      *gio_stream_new_from_uri       (const gchar   *uri);
GioStream      *gio_stream_new                (const gchar   *stream_type_name,
					       const gchar   *first_property_name,
					       ...);
GioStream      *gio_stream_newv               (const gchar   *stream_type_name,
					       guint          n_parameters,
					       GParameter    *parameters);
GioStream      *gio_stream_new_valist         (const gchar   *stream_type_name,
					       const gchar   *first_property_name,
					       va_list        var_args);

/* Public Methods */
GioStreamFlags  gio_stream_get_flags          (GioStream     *stream);
gssize          gio_stream_get_size           (GioStream     *stream);

void            gio_stream_freeze             (GioStream     *stream);
void            gio_stream_thaw               (GioStream     *stream);

gboolean        gio_stream_open               (GioStream     *stream,
					       GioError     **error);
gboolean        gio_stream_close              (GioStream     *stream,
					       GioError     **error);

gboolean        gio_stream_read               (GioStream     *stream,
					       GioBuffer    **buffer,
					       gssize         read_size,
					       GioError     **error);
GioBuffer      *gio_stream_peek               (GioStream     *stream);

gboolean        gio_stream_write              (GioStream     *stream,
					       GioBuffer     *buffer,
					       gsize         *bytes_written,
					       GioError     **error);
void            gio_stream_flush              (GioStream     *stream);

gssize          gio_stream_seek               (GioStream     *stream,
					       gssize         offset,
					       GSeekType      whence,
					       GioError     **error);
gssize          gio_stream_tell               (GioStream     *stream);

/* Properties */
gint            gio_stream_get_state          (GioStream     *stream);

GioEventType    gio_stream_get_event_mask     (GioStream     *stream);
void            gio_stream_set_event_mask     (GioStream     *stream,
					       GioEventType   events);

GioStream      *gio_stream_get_parent         (GioStream     *stream);
void            gio_stream_set_parent         (GioStream     *stream,
					       GioStream     *parent);

/* Event-Signal Emission */
gboolean        gio_stream_event              (GioStream     *stream,
					       GioEvent      *event);
gboolean        gio_stream_readable_event     (GioStream     *stream);
gboolean        gio_stream_writable_event     (GioStream     *stream);
void            gio_stream_error_event        (GioStream     *stream,
					       GSList        *error_list);
void            gio_stream_error_event_single (GioStream     *stream,
					       GioError      *error);
void            gio_stream_eof_event          (GioStream     *stream);
void            gio_stream_state_event        (GioStream     *stream,
					       gint           new_state);

/* Common Errors */
GQuark gio_stream_error_get_quark (void) G_GNUC_CONST;
void   gio_set_stream_error       (GioError       **error,
				   GioStreamError   code,
				   const gchar     *format,
				   ...) G_GNUC_PRINTF (3, 4);

G_END_DECLS

#endif /* !__GIO_STREAM_H__ */
