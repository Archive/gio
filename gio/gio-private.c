/*
 * Gio Library: gio/gio-private.c
 *
 * Copyright (c) 2004 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include "gio-private.h"

#ifndef G_TYPE_ERROR
static gpointer
copy_error (gpointer src)
{
  if (src)
    return g_error_copy (src);

  return NULL;
}

GType
_gio_gerror_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (type == G_TYPE_INVALID)
    {
      type = g_boxed_type_register_static ("GError", (GBoxedCopyFunc) copy_error,
					   (GBoxedFreeFunc) g_error_free);
    }

  return type;
}
#endif /* !G_TYPE_ERROR */
