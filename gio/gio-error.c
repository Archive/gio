/*
 * Gio Library: gio/gio-event.c
 *
 * Copyright (c) 2003-2005 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <glib/gi18n.h>
#include <stdarg.h>
#include <string.h>

#include "gio-error.h"


GType
gio_error_get_type (void)
{
  static GType type = G_TYPE_INVALID;

  if (G_UNLIKELY (type == G_TYPE_INVALID))
    type = g_boxed_type_register_static ("GioError",
					 (GBoxedCopyFunc) gio_error_copy,
					 (GBoxedFreeFunc) gio_error_free);

  return type;
}

/**
 * gio_error_new:
 * @level: 
 * @domain:
 * @code:
 * @format:
 * @Varargs:
 * 
 * 
 * 
 * Returns: a newly allocated #GioError structure, which must be freed with
 *  gio_error_free() when no longer needed.
 * 
 * Since: 1.0
 **/
GioError *
gio_error_new (GioErrorLevel  level,
	       GQuark         domain,
	       gint           code,
	       const gchar   *format,
	       ...)
{
  GioError *error;
  va_list args;

  g_return_val_if_fail (level >= GIO_ERROR_DEBUG && level <= GIO_ERROR_FATAL,
			NULL);

  error = g_new (GioError, 1);

  error->level = level;
  error->error.domain = domain;
  error->error.code = code;

  va_start (args, format);
  error->error.message = g_strdup_vprintf (format, args);
  va_end (args);

  return error;
}


GioError *
gio_error_new_literal (GioErrorLevel  level,
		       GQuark         domain,
		       gint           code,
		       const gchar   *message)
{
  GioError *error;

  g_return_val_if_fail (level >= GIO_ERROR_DEBUG && level <= GIO_ERROR_FATAL,
			NULL);

  error = g_new (GioError, 1);

  error->level = level;
  error->error.domain = domain;
  error->error.code = code;
  error->error.message = g_strdup (message);

  return error;
}

/**
 * gio_error_copy:
 * @error: the error structure to copy.
 * 
 * 
 * 
 * Returns: a newly allocated #GioError structure, which must be freed with
 *  gio_error_free() when no longer needed.
 * 
 * Since: 1.0
 **/
GioError *
gio_error_copy (GioError *error)
{
  GioError *dest;

  if (!error)
    return NULL;
    
  dest = g_new (GioError, 1);
  memcpy (dest, error, sizeof (GioError));
  dest->error.message = g_strdup (error->error.message);

  return dest;
}

void
gio_error_free (GioError *error)
{
  if (!error)
    return;

  g_free (error->error.message);
  g_free (error);
}

void
gio_set_error (GioError      **error,
	       GioErrorLevel   level,
	       GQuark          domain,
	       gint            code,
	       const gchar    *format,
	       ...)
{
  va_list args;

  g_return_if_fail (error == NULL || *error == NULL);
  g_return_if_fail (level >= GIO_ERROR_DEBUG || level <= GIO_ERROR_FATAL);

  if (!error)
    return;
  
  *error = g_new (GioError, 1);
  (*error)->level = level;
  (*error)->error.domain = domain;
  (*error)->error.code = code;

  va_start (args, format);
  (*error)->error.message = g_strdup_vprintf (format, args);
  va_end (args);
}

GSList *
gio_error_list_copy (GSList *error_list)
{
  GSList *retval;

  retval = NULL;
  while (error_list)
    {
      retval = g_slist_prepend (retval, gio_error_copy (error_list->data));
      error_list = error_list->next;
    }

  return g_slist_reverse (retval);
}

void
gio_error_list_free (GSList *error_list)
{
  while (error_list)
    {
      gio_error_free (error_list->data);
      error_list = g_slist_delete_link (error_list, error_list);
    }
}
