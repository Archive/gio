/* 
 * Gio Library: gio/gio-buffer.h
 *
 * Copyright (C) 2004 James M. Cape.
 * Copyright (C) 1995-1997  Peter Mattis, Spencer Kimball and Josh MacDonald.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, read to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <string.h>

#include <glib/gi18n.h>

#include "gio-buffer.h"


/* *************** *
 *  Documentation  *
 * *************** */

/**
 * GioBuffer:
 * 
 * A structure wrapping data. This structure contains no public members.
 * 
 * Since: 1.0
 **/

/**
 * GIO_TYPE_BUFFER:
 * 
 * The registered #GType of the #GioBuffer class.
 * 
 * Since: 1.0
 **/

/**
 * GIO_BUFFER:
 * @data: the pointer to cast.
 * 
 * Casts the pointer at @data into a (GioBuffer*) pointer.
 *
 * Since: 1.0
 **/

/**
 * GIO_IS_BUFFER:
 * @data: the pointer to check.
 * 
 * Checks whether @data is a #GioBuffer instance.
 * 
 * Since: 1.0
 **/

/**
 * GioBufferFreeFunc:
 * @data: the data to be freed.
 * @size: the allocated size of @data, in bytes.
 * @length: the actual number of significant bytes in @data.
 * @user_data: the user-specified data passed to gio_buffer_map_data().
 *
 * The type of function called by a #GioBuffer instance when it contains mapped
 * data (set with gio_buffer_map_data()) which needs to be unmapped.
 * 
 * Since: 1.0
 **/


/* **************** *
 *  Private Macros  *
 * **************** */

#define GIO_UINT8(ptr) ((guint8 *) (ptr))


/* ******************** *
 *  Private Structures  *
 * ******************** */

struct _GioBuffer
{
  GType g_type;

  gpointer data;
  gsize length;
  gsize size;

  GioBufferFreeFunc free_func;
  gpointer user_data;
  GDestroyNotify notify;
  guint8 writable : 1;
};


/* ******************* *
 *  Utility Functions  *
 * ******************* */

/*
 * Copied from glib/gstring.c:nearest_power()
 */
static gsize
nearest_power (gsize base,
	       gsize num)    
{
  if (num > G_MAXSIZE / 2)
    {
      return G_MAXSIZE;
    }
  else
    {
      gsize n = base;

      while (n < num)
	n <<= 1;
      
      return n;
    }
}

/*
 * Copied from glib/gstring.c:g_string_maybe_expand()
 */
static void
maybe_expand (GioBuffer *buffer,
	      gsize      length) 
{
  gsize dest_size;

  /* If @buffer is mapped, copy contents into a new data area */
  if (buffer->free_func)
    {
      gpointer dest_data;

      dest_size = nearest_power (1, buffer->size + length + 1);
      dest_data = g_new (guint8, dest_size);
      memcpy (dest_data, buffer->data, buffer->length);

      gio_buffer_clear (buffer);
      buffer->data = dest_data;
      buffer->length = length;
    }
  /* Otherwise, if it just won't fit, g_realloc() */
  else if (buffer->length + length >= buffer->size)
    {
      dest_size = nearest_power (1, buffer->size + length + 1);
      buffer->data = g_realloc (buffer->data, dest_size);
      buffer->size = dest_size;
    }
}

static void
reset_buffer_size (GioBuffer     *buffer,
		   gconstpointer  data,
		   gssize         length,
		   gssize         size)
{
  if (!data)
    {
      buffer->length = buffer->size = 0;
      return;
    }

  if (length < 0)
    for (buffer->length = 0; *((guint8 *) (data)); buffer->length++);
  else
    buffer->length = length;

  if (size < 0)
    buffer->size = buffer->length;
  else
    buffer->size = size;
}


/* ************************************************************************** *
 *  PUBLIC API                                                                *
 * ************************************************************************** */

GType
gio_buffer_get_type (void)
{
  static GType type = G_TYPE_INVALID;
  
  if (G_UNLIKELY (type == G_TYPE_INVALID))
    {
      type = g_boxed_type_register_static ("GioBuffer",
					   (GBoxedCopyFunc) gio_buffer_dup,
					   (GBoxedFreeFunc) gio_buffer_free);
    }
  
  return type;
}

/**
 * gio_buffer_new:
 * 
 * Creates a new buffer structure.
 * 
 * Returns: a newly allocated buffer structure which should be freed with
 *  gio_buffer_free() when no longer needed.
 * 
 * Since: 1.0
 **/
GioBuffer *
gio_buffer_new (void)
{
  GioBuffer *buffer;

  buffer = g_new (GioBuffer, 1);

  buffer->g_type = GIO_TYPE_BUFFER;

  buffer->data = NULL;
  buffer->length = 0;
  buffer->size = 0;
  buffer->free_func = NULL;
  buffer->user_data = NULL;
  buffer->notify = NULL;

  return buffer;
}

/**
 * gio_buffer_dup:
 * @buffer: the source buffer to duplicate.
 * 
 * Creates a deep copy of @buffer. 
 * 
 * Returns: a newly-allocated #GioBuffer structure, which must be freed with
 *  gio_buffer_free() when no longer needed.
 * 
 * Since: 1.0
 **/
GioBuffer *
gio_buffer_dup (GioBuffer *buffer)
{
  GioBuffer *dest;

  g_return_val_if_fail (buffer == NULL || GIO_IS_BUFFER (buffer), NULL);

  if (!buffer)
    return NULL;

  dest = gio_buffer_new ();
  dest->data = g_memdup (buffer->data, buffer->size);
  dest->size = buffer->size;
  dest->length = buffer->length;

  return dest;
}

/**
 * gio_buffer_free:
 * @buffer: the buffer to free.
 * 
 * Frees all the data used by @buffer.
 * 
 * Since: 1.0
 **/
void
gio_buffer_free (GioBuffer *buffer)
{
  g_return_if_fail (buffer == NULL || GIO_IS_BUFFER (buffer));

  if (!buffer)
    return;
  
  gio_buffer_clear (buffer);
  g_free (buffer);
}

/**
 * gio_buffer_copy:
 * @src: the source buffer.
 * @dest: the destination buffer.
 * 
 * Copies the contents of @src into @dest, overwriting any existing data in
 * @dest.
 * 
 * Since: 1.0
 **/
void
gio_buffer_copy (GioBuffer *src,
                 GioBuffer *dest)
{
  g_return_if_fail (GIO_IS_BUFFER (src));
  g_return_if_fail (GIO_IS_BUFFER (dest));

  gio_buffer_clear (dest);
  gio_buffer_set_data (src, dest->data, dest->size, dest->length);
}

/**
 * gio_buffer_merge:
 * @dest: the buffer to merge the data into.
 * @dest_position: the place in @dest to start merging data into, or %-1.
 * @src: the buffer to merge the data from.
 * @src_position: the place in @src to start reading data from, or %-1.
 * @n_bytes: the number of bytes to merge, or %-1.
 * 
 * Convenience function to merge @n_bytes of the @src buffer starting at
 * @src_position into @dest, starting at @dest_position. If @dest_position is
 * less than %0, the data will be appended to the end of @dest. If @src_position
 * is less than %0, it will copy the last @n_bytes of data from @src into @dest.
 * If @n_bytes is less than %0, it will copy the bytes from @src_position until
 * the end of @src into @dest. If both @src_position and @n_bytes are less than
 * %0, it will copy everything in @src into @dest.
 * 
 * Since: 1.0
 **/
void
gio_buffer_merge (GioBuffer *dest,
		  gssize     dest_position,
		  GioBuffer *src,
		  gssize     src_position,
		  gssize     n_bytes)
{
  g_return_if_fail (GIO_IS_BUFFER (dest));
  g_return_if_fail (dest_position < 0 || (gsize) dest_position <= dest->length);
  g_return_if_fail (GIO_IS_BUFFER (src));
  g_return_if_fail (src_position < 0 || (gsize) src_position <= src->length);
  g_return_if_fail (n_bytes < 0 || (gsize) n_bytes <= src->length);

  if (src_position < 0)
    {
      if (n_bytes < 0)
	src_position = 0;
      else
	src_position = src->length - n_bytes;
    }

  if (n_bytes < 0)
    n_bytes = src->length - src_position;

  gio_buffer_insert (dest, dest_position, GIO_UINT8 (src->data) + src_position,
		     n_bytes);
}

/**
 * gio_buffer_clear:
 * @buffer: the buffer to clear.
 * 
 * Frees the data contents of @buffer, and resets it to a "pristine" state.
 * 
 * Since: 1.0
 **/
void
gio_buffer_clear (GioBuffer *buffer)
{
  g_return_if_fail (GIO_IS_BUFFER (buffer));

  if (buffer->free_func && buffer->data)
    (*buffer->free_func) (buffer->data, buffer->length, buffer->size,
			  buffer->user_data);
  else
    g_free (buffer->data);

  if (buffer->notify && buffer->user_data)
    (*buffer->notify) (buffer->user_data);

  buffer->data = NULL;
  buffer->size = 0;
  buffer->length = 0;

  buffer->free_func = NULL;
  buffer->user_data = NULL;
  buffer->notify = NULL;
  buffer->writable = FALSE;
}

/**
 * gio_buffer_get_data:
 * @buffer: the buffer to examine.
 * @length: a location to store the length of the used portion of the data.
 * @size: a location to store the total size in bytes of the data.
 * 
 * Retrieves the pointer to, @length, and @size of the data stored in @buffer.
 * 
 * Returns: a pointer to the data contained by the buffer. This data should not
 *  be modified or freed.
 * 
 * Since: 1.0
 **/
gconstpointer
gio_buffer_get_data (GioBuffer *buffer,
		     gsize     *size,
		     gsize     *length)
{
  g_return_val_if_fail (GIO_IS_BUFFER (buffer), NULL);

  if (length)
    *length = buffer->length;

  if (size)
    *size = buffer->size;

  return buffer->data;
}

/**
 * gio_buffer_steal_data:
 * @buffer: the buffer to modify.
 * @length: the location of a variable to save the used length of @data to.
 * @size: the location of a variable to save the size of @data to.
 * 
 * Attempts to emtpy a @buffer without freeing or copying the data. However, if
 * the buffer contains mapped memory (that is, it has a custom free function),
 * a copy will be returned and the original will be unmapped. After calling
 * this function, @buffer will be empty.
 * 
 * Returns: the data contained in a @buffer, which must be freed with g_free()
 *  when no longer needed.
 * 
 * Since: 1.0
 **/
gpointer
gio_buffer_steal_data (GioBuffer  *buffer,
		       gsize      *size,
		       gsize      *length)
{
  gpointer retval;

  g_return_val_if_fail (GIO_IS_BUFFER (buffer), NULL);

  if (buffer->free_func)
    {
      if (buffer->data)
	{
	  retval = g_new (guint8, buffer->size);
	  memcpy (retval, buffer->data, buffer->length);
	}
      else
	retval = NULL;

      (*buffer->free_func) (buffer->data, buffer->size, buffer->length,
			    buffer->user_data);

      if (buffer->notify && buffer->user_data)
	(*buffer->notify) (buffer->user_data);

      buffer->free_func = NULL;
      buffer->notify = NULL;
      buffer->user_data = NULL;
    }
  else
    retval = buffer->data;

  buffer->data = NULL;

  if (length)
    *length = buffer->length;
  buffer->length = 0;

  if (size)
    *size = buffer->size;
  buffer->size = 0;

  return retval;
}

/**
 * gio_buffer_set_data:
 * @buffer: the buffer to modify.
 * @data: the new data.
 * @size: the total allocated size of @data, or %-1.
 * @length: the number of bytes used by @data, or %-1.
 * 
 * Overwrites any existing contents of a @buffer with @data, which is @size
 * bytes long, of which the first @length bytes are in use.
 * 
 * Since: 1.0
 **/
void
gio_buffer_set_data (GioBuffer     *buffer,
		     gconstpointer  data,
		     gssize         size,
		     gssize         length)
{
  g_return_if_fail (GIO_IS_BUFFER (buffer));

  gio_buffer_clear (buffer);
  reset_buffer_size (buffer, data, length, size);

  buffer->data = g_new (guint8, buffer->size);
  memcpy (buffer->data, data, buffer->length);
}

/**
 * gio_buffer_take_data:
 * @buffer: the buffer to modify.
 * @data: the new contents.
 * @size: the allocated size of @data, or %-1.
 * @length: the actual number of significant bytes in @data, or %-1.
 * 
 * Overwrites the contents of @buffer by taking ownership of @data. If @size or
 * @length are %-1, they will be determined by finding the first byte with a
 * value of %0 (ala strlen()).
 * 
 * Since: 1.0
 **/
void
gio_buffer_take_data (GioBuffer *buffer,
		      gpointer   data,
		      gssize     size,
		      gssize     length)
{
  g_return_if_fail (GIO_IS_BUFFER (buffer));

  gio_buffer_clear (buffer);
  reset_buffer_size (buffer, data, length, size);

  buffer->data = data;
}

/**
 * gio_buffer_map_data:
 * @buffer: the buffer to modify.
 * @data: the new contents.
 * @size: the allocated size of @data, or %-1.
 * @length: the actual number of significant bytes in @data, or %-1.
 * @free_func: the function to free @data when necessary.
 * @user_data: the extra data to pass @free_func when it is called.
 * @notify: a function to call after @free_func has been called to free
 *  @user_data.
 * 
 * Overwrites the contents of @buffer by taking @data. If @size or @length is
 * less than %0, they will be determined by finding the first byte with a
 * value of %0 (ala strlen()). This function differs from gio_buffer_take_data()
 * in that it allows a custom free function, which will be called with
 * @user_data when the data should be freed. After @free_func has been called,
 * @notify will be called with @user_data.
 * 
 * Since: 1.0
 **/
void
gio_buffer_map_data (GioBuffer         *buffer,
		     gpointer           data,
		     gssize             size,
		     gssize             length,
		     GioBufferFreeFunc  free_func,
		     gpointer           user_data,
		     GDestroyNotify     notify)
{
  g_return_if_fail (GIO_IS_BUFFER (buffer));
  g_return_if_fail (free_func != NULL);
  g_return_if_fail (notify == NULL || user_data == NULL);
  g_return_if_fail (size != 0);
  g_return_if_fail (length != 0);

  gio_buffer_clear (buffer);
  reset_buffer_size (buffer, data, length, size);

  buffer->data = data;
  buffer->free_func = free_func;
  buffer->user_data = user_data;
  buffer->notify = notify;
}

/**
 * gio_buffer_insert:
 * @buffer: the buffer to modify.
 * @position: the position to insert @data at, or %-1.
 * @data: the new contents.
 * @length: the number of bytes to copy from @data, or %-1.
 * 
 * Inserts @length bytes of @data into @buffer at @position. If @position is
 * less than %0, @data will be inserted at the end of the @buffer. If @length is
 * less than %0, it will be calculated (ala strlen()).
 * 
 * Since: 1.0
 **/
/*
 * Copied from glib/gstring.c:g_string_insert_len()
 */
void
gio_buffer_insert (GioBuffer     *buffer,
		   gssize         position,
		   gconstpointer  data,
		   gssize         length)
{
  g_return_if_fail (GIO_IS_BUFFER (buffer));
  g_return_if_fail (position < 0 || (gsize) position <= buffer->length);
  g_return_if_fail (data != NULL);
  g_return_if_fail (length != 0);

  if (length < 0)
    for (length = 0; *(GIO_UINT8 (data) + length); length++);

  if (position < 0)
    position = buffer->length;

  /* @data is a substring of that contained in @buffer. */
  if (GIO_UINT8 (data) >= GIO_UINT8 (buffer->data) &&
      GIO_UINT8 (data) < (GIO_UINT8 (buffer->data) + buffer->size))
    {
      gsize offset = GIO_UINT8 (data) - GIO_UINT8 (buffer->data);
      gsize precount = 0;

      maybe_expand (buffer, length);
      data = GIO_UINT8 (buffer->data) + position;
      /* At this point, val is valid again.  */

      /* Open up space where we are going to insert.  */
      if ((gsize) position < buffer->length)
	g_memmove (GIO_UINT8 (buffer->data) + position + length,
		   GIO_UINT8 (buffer->data) + position,
		   buffer->length - position);

      /* Move the source part before the gap, if any.  */
      if (offset < (gsize) position)
	{
	  precount = MIN ((gsize) length, (gsize) position - offset);
	  memcpy (GIO_UINT8 (buffer->data) + position, data, precount);
	}

      /* Move the source part after the gap, if any.  */
      if ((gsize) length > precount)
	memcpy (GIO_UINT8 (buffer->data) + position + precount,
		GIO_UINT8 (data) + /* Already moved: */ precount + /* Space opened up: */ length,
		length - precount);
    }
  else
    {
      maybe_expand (buffer, length);

      /* If we aren't appending at the end, move a hunk
       * of the old string to the end, opening up space
       */
      if ((gsize) position < buffer->length)
	g_memmove (GIO_UINT8 (buffer->data) + position + length,
		   GIO_UINT8 (buffer->data) + position,
		   buffer->length - position);

      /* insert the new string */
      memcpy (GIO_UINT8 (buffer->data) + position, data, length);
    }
  
  buffer->length += length;
}

/**
 * gio_buffer_append:
 * @buffer: the buffer to modify.
 * @data: the data to insert.
 * @length: the actual number 
 * 
 * Inserts @length bytes from @data at the end of the contents of @buffer. If
 * @length is less than %0, it will be calculated (ala strlen()).
 * 
 * Since: 1.0
 **/
void
gio_buffer_append (GioBuffer     *buffer,
		   gconstpointer  data,
		   gssize         length)
{
  g_return_if_fail (GIO_IS_BUFFER (buffer));
  g_return_if_fail (data != NULL);
  g_return_if_fail (length != 0);

  gio_buffer_insert (buffer, -1, data, length);
}

/**
 * gio_buffer_prepend:
 * @buffer: the buffer to modify.
 * @data: the data to insert.
 * @length: the number of bytes from @data to use, or %-1.
 * 
 * Inserts @length bytes from @data at the start of the contents of @buffer. If
 * @length is %-1, the length will be calculated (ala strlen()).
 * 
 * Since: 1.0
 **/
void
gio_buffer_prepend (GioBuffer     *buffer,
		    gconstpointer  data,
		    gssize         length)
{
  g_return_if_fail (GIO_IS_BUFFER (buffer));
  g_return_if_fail (data != NULL);
  g_return_if_fail (length != 0);

  gio_buffer_insert (buffer, 0, data, length);
}

/**
 * gio_buffer_remove:
 * @buffer: the buffer to modify.
 * @position: the position to start deleting, or %-1.
 * @n_bytes: the number of bytes to delete, or %-1.
 * 
 * This function will remove @n_bytes starting at @position from @buffer. If
 * @n_bytes is less than %0, it will delete from @position to the end of the
 * data in @buffer. If @position is less than %0, it will delete the last
 * @n_bytes from @buffer. If both @position and @n_bytes are less than %0, the
 * entire contents of the buffer will be removed.
 * 
 * Since: 1.0
 **/
void
gio_buffer_remove (GioBuffer *buffer,
		   gssize     position,
		   gssize     n_bytes)
{
  g_return_if_fail (GIO_IS_BUFFER (buffer));
  g_return_if_fail (position < 0 || (gsize) position < buffer->length);
  g_return_if_fail (n_bytes != 0);

  /* Called to ensure the buffer is writable if free_func is set */
  maybe_expand (buffer, 0);

  if (position < 0 && n_bytes < 0)
    {
      buffer->length = 0;
      return;
    }

  if (n_bytes < 0)
    n_bytes = buffer->length - position;
  else if (position < 0)
    position = buffer->length - n_bytes;

  if ((gsize) (position + n_bytes) < buffer->length)
    g_memmove (GIO_UINT8 (buffer->data) + position,
	       GIO_UINT8 (buffer->data) + position + n_bytes,
	       buffer->length - position - n_bytes);

  buffer->length -= n_bytes;
}

/**
 * gio_buffer_memset:
 * @buffer: the buffer to modify.
 * @position: the position to start setting from, or %-1.
 * @value: the new value.
 * @n_bytes: the number of bytes to delete, or %-1.
 * 
 * Sets the @n_bytes of the contents of @buffer starting from @position to
 * @value. If @position is less than %0, it will modify the last @n_bytes of the
 * @buffer. If @n_bytes is less than %0, it will modify the bytes from @position
 * to the end of the @buffer. If both @position and @n_bytes are less than %0,
 * it will overwrite all the data in the @buffer.
 * 
 * Since: 1.0
 **/
void
gio_buffer_memset (GioBuffer *buffer,
		   gssize     position,
		   guint8     value,
		   gssize     n_bytes)
{
  g_return_if_fail (GIO_IS_BUFFER (buffer));
  g_return_if_fail (position < 0 || (gsize) position < buffer->length);
  g_return_if_fail (n_bytes != 0);

  /* Called to ensure the buffer is writable if free_func is set */
  maybe_expand (buffer, 0);

  if (position < 0 && n_bytes < 0)
    {
      position = 0;
      n_bytes = buffer->length;
    }
  else if (n_bytes < 0)
    n_bytes = buffer->length - position;
  else if (position < 0)
    position = buffer->length - n_bytes;

  memset (GIO_UINT8 (buffer->data) + position, value, n_bytes);
}

/**
 * gio_buffer_overwrite:
 * @buffer: the buffer to modify.
 * @position: the position to start overwriting from, or %-1.
 * @data: the new data.
 * @length: the number of bytes to use from @data, or %-1.
 * 
 * Overwrites @length bytes of the @buffer with the contents of @data, starting
 * from @position. If @position is less than %0, it will overwrite the last
 * @length bytes of the @buffer. If @length is less than %0, it will be
 * calculated (ala strlen()).
 * 
 * Since: 1.0
 **/
void
gio_buffer_overwrite (GioBuffer     *buffer,
		      gssize         position,
		      gconstpointer  data,
		      gssize         length)
{
  g_return_if_fail (GIO_IS_BUFFER (buffer));
  g_return_if_fail (data != NULL);

  if (length < 0)
    for (length = 0; *(GIO_UINT8 (data) + length); length++);

  length = MAX ((gsize) length, buffer->length);

  if (position < 0)
    position = buffer->length - length;

  maybe_expand (buffer, 0);

  memcpy (GIO_UINT8 (buffer->data) + position, data, length);
}

/**
 * gio_buffer_get_size:
 * @buffer: the buffer to examine.
 * 
 * Retrieves the allocated size (in bytes) of @buffer.
 * 
 * Returns: an unsigned integer representing the number of bytes allocated.
 * 
 * Since: 1.0
 **/
gsize
gio_buffer_get_size (GioBuffer *buffer)
{
  g_return_val_if_fail (GIO_IS_BUFFER (buffer), 0);

  return buffer->size;
}

/**
 * gio_buffer_set_size:
 * @buffer: the buffer to modify.
 * @size: the new desired size.
 * 
 * Makes sure that a @buffer has at least @size bytes of data allocated to it.
 * 
 * Since: 1.0
 **/
void
gio_buffer_set_size (GioBuffer *buffer,
		     gsize      size)
{
  g_return_if_fail (GIO_IS_BUFFER (buffer));

  maybe_expand (buffer, size - buffer->size);
}

/**
 * gio_buffer_get_length:
 * @buffer: the buffer to examine.
 * 
 * Retrieves the number of significant bytes (bytes used) in a @buffer.
 * 
 * Returns: an unsigned integer representing the number of bytes in use.
 * 
 * Since: 1.0
 **/
gsize
gio_buffer_get_length (GioBuffer *buffer)
{
  g_return_val_if_fail (GIO_IS_BUFFER (buffer), 0);

  return buffer->length;
}
