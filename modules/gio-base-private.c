/* 
 * Gio Library: modules/gio-base-private.c
 *
 * Copyright (C) 2005 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>

#include <gio/gio-stream.h>

#include "gio-base-private.h"

GIOCondition
_gio_base_events_to_condition (GioEventType events)
{
  GIOCondition cond;

  cond = 0;

  if (events & GIO_EVENT_STATE)
    cond = (G_IO_HUP | G_IO_ERR);

  if (events & GIO_EVENT_READABLE)
    cond |= G_IO_IN;
  
  if (events & GIO_EVENT_WRITABLE)
    cond |= G_IO_OUT;

  if (events & GIO_EVENT_EOF)
    cond |= G_IO_HUP;

  if (events & GIO_EVENT_ERROR)
    cond |= G_IO_ERR;
  
  return cond;
}

gboolean
_gio_base_io_callback (GIOCondition cond,
		       gpointer     data)
{
  GioEventType events;
  gboolean retval;

  g_assert (GIO_IS_STREAM (data));

  GIO_OBJECT_LOCK (data);

  events = gio_stream_get_event_mask (data);
  retval = FALSE;

  if ((((events & GIO_EVENT_READABLE) == GIO_EVENT_READABLE)) &&
      ((((cond & G_IO_IN) == G_IO_IN)) || ((cond & G_IO_PRI) == G_IO_PRI)))
    {
      gio_stream_readable_event (data);
      retval = TRUE;
    }
  else if (((events & GIO_EVENT_READABLE) == GIO_EVENT_READABLE) &&
	   ((cond & G_IO_OUT) == G_IO_OUT))
    {
      gio_stream_readable_event (data);
      retval = TRUE;
    }
  else if (((events & GIO_EVENT_EOF) == GIO_EVENT_EOF) &&
	   ((cond & G_IO_HUP) == G_IO_HUP))
    {
      gio_stream_eof_event (data);
      retval = FALSE;
    }
  else if (((events & GIO_EVENT_ERROR) == GIO_EVENT_ERROR) && 
	   ((cond & G_IO_ERR) == G_IO_ERR))
    {
      GSList *error_list;
    
      error_list = NULL;
      gio_stream_error_event (data, error_list);
      gio_error_list_free (error_list);
      retval = FALSE;
    }

  GIO_OBJECT_UNLOCK (data);

  return retval;
}
