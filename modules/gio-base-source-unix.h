/* 
 * Gio Library: gio/gio-source.h
 *
 * Copyright (C) 2004-2005 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * Internal GSource for UNIX platforms.
 */

#ifndef __GIO_BASE_SOURCE_UNIX_H__
#define __GIO_BASE_SOURCE_UNIX_H__ 1

#include <glib.h>

G_BEGIN_DECLS

#define GIO_BASE_SOURCE_UNIX(ptr)	((GioBaseSourceUnix *)(ptr))

typedef struct _GioBaseSourceUnix GioBaseSourceUnix;

typedef gboolean (*GioBaseSourceFunc) (GIOCondition  conditions,
				       gpointer      user_data);

GSource      *_gio_base_source_unix_new           (gint               fd,
						   GIOCondition       listen_for);

GIOCondition  _gio_base_source_unix_get_condition (GioBaseSourceUnix *source);
void          _gio_base_source_unix_set_condition (GioBaseSourceUnix *source,
						   GIOCondition       listen_for);

G_END_DECLS

#endif /* !__GIO_BASE_SOURCE_UNIX_H__ */
