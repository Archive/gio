/* 
 * Gio Library: modules/gio-file-module.c
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, read to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gio/gio-stream.h>

#include "gio-file-stream.h"
#include "gio-base-type-builtins.h"

void module_init       (GTypeModule  *module);
void module_finalize   (GTypeModule  *module);
void module_list_types (gint         *num_types,
			const GType **types);

void
module_init (GTypeModule *module)
{
  gio_file_stream_register_type (module);
  gio_file_flags_register_type (module);
  gio_file_permission_flags_register_type (module);
  gio_file_error_register_type (module);
}

void
module_finalize (GTypeModule *module)
{
}

void
module_list_types (int          *num_types,
		   const GType **types)
{
  static GType types_a[4];

  types_a[0] = GIO_TYPE_FILE_STREAM;
  types_a[1] = GIO_TYPE_FILE_FLAGS;
  types_a[2] = GIO_TYPE_FILE_PERMISSION_FLAGS;
  types_a[3] = GIO_TYPE_FILE_ERROR;

  *types = types_a;
  *num_types = G_N_ELEMENTS (types_a);
}
