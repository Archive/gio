/* 
 * Gio Library: gio/gio-file-stream.h
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GIO_FILE_STREAM_H__
#define __GIO_FILE_STREAM_H__ 1

#include <gio/gio-stream.h>

G_BEGIN_DECLS

#if (defined G_OS_UNIX)
typedef gint GioFileHandle;
#define GIO_FILE_HANDLE_INVALID -1
#elif (defined G_OS_WIN32)
typedef HANDLE GioFileHandle;
#define GIO_FILE_HANDLE_INVALID NULL
#else /* !G_OS_UNIX && !G_OS_WIN32 */
#error Unsupported operating system
#endif /* !G_OS_UNIX && !G_OS_WIN32 */

#define GIO_TYPE_FILE_STREAM \
  (gio_file_stream_get_type ())
#define GIO_FILE_STREAM(object) \
  (G_TYPE_CHECK_INSTANCE_CAST ((object), GIO_TYPE_FILE_STREAM, GioFileStream))
#define GIO_FILE_STREAM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), GIO_TYPE_FILE_STREAM, GioFileStreamClass))
#define GIO_IS_FILE_STREAM(object) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((object), GIO_TYPE_FILE_STREAM))
#define GIO_IS_FILE_STREAM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), GIO_TYPE_FILE_STREAM))
#define GIO_FILE_STREAM_GET_CLASS(object) \
  (G_TYPE_INSTANCE_GET_CLASS ((object), GIO_TYPE_FILE_STREAM, GioFileStreamClass))

#define GIO_FILE_ERROR \
  (gio_file_error_get_quark ())


typedef enum /* <flags,prefix=GIO_FILE_STREAM> */
{
  GIO_FILE_READABLE  = 1 << 0,
  GIO_FILE_WRITABLE  = 1 << 1,
  GIO_FILE_CREATE    = 1 << 2,
  GIO_FILE_NEW_ONLY  = 1 << 3,
  GIO_FILE_TRUNCATE  = 1 << 4,
  GIO_FILE_APPEND    = 1 << 5,

  GIO_FILE_READWRITE = 0x2
}
GioFileFlags;


typedef enum /* <flags,prefix=GIO_FILE_PERMISSION> */
{
  GIO_FILE_PERMISSION_NONE		= 0,

  GIO_FILE_PERMISSION_OTHER_EXECUTABLE	= 1 << 0,
  GIO_FILE_PERMISSION_OTHER_WRITABLE	= 1 << 1,
  GIO_FILE_PERMISSION_OTHER_READABLE	= 1 << 2,

  GIO_FILE_PERMISSION_GROUP_EXECUTABLE	= 1 << 3,
  GIO_FILE_PERMISSION_GROUP_WRITABLE	= 1 << 4,
  GIO_FILE_PERMISSION_GROUP_READABLE	= 1 << 5,

  GIO_FILE_PERMISSION_OWNER_EXECUTABLE	= 1 << 6,
  GIO_FILE_PERMISSION_OWNER_WRITABLE	= 1 << 7,
  GIO_FILE_PERMISSION_OWNER_READABLE	= 1 << 8,

  GIO_FILE_PERMISSION_SET_USER		= 1 << 9,
  GIO_FILE_PERMISSION_SET_GROUP		= 1 << 10,
  GIO_FILE_PERMISSION_STICKY		= 1 << 11
}
GioFilePermissionFlags;


typedef enum /* <prefix=GIO_FILE_STREAM_ERROR> */
{
  GIO_FILE_ERROR_ALREADY_EXISTS,
  GIO_FILE_ERROR_DOESNT_EXIST,
  GIO_FILE_ERROR_BAD_PATH,
  GIO_FILE_ERROR_READ_PERMISSIONS,
  GIO_FILE_ERROR_WRITE_PERMISSIONS,
  GIO_FILE_ERROR_FILE_BUSY,
  GIO_FILE_ERROR_TOO_MANY_FILES,
  GIO_FILE_ERROR_DISK_ERROR,
  GIO_FILE_ERROR_INTERNAL
}
GioFileError;


typedef struct _GioFileStream GioFileStream;
typedef struct _GioFileStreamClass GioFileStreamClass;
typedef struct _GioFileStreamPrivate GioFileStreamPrivate;


struct _GioFileStream
{
  /* <private> */
  GioStream parent;
  
  GioFileStreamPrivate *priv;
};

struct _GioFileStreamClass
{
  /* <private> */
  GioStreamClass parent_class;

  void (*__gio_reserved1);
  void (*__gio_reserved2);
  void (*__gio_reserved3);
  void (*__gio_reserved4);
  void (*__gio_reserved5);
  void (*__gio_reserved6);
  void (*__gio_reserved7);
  void (*__gio_reserved8);
};

void                   gio_file_stream_register_type   (GTypeModule *module);
GType                  gio_file_stream_get_type        (void) G_GNUC_CONST;

G_CONST_RETURN gchar  *gio_file_stream_get_filename    (GioFileStream          *fstream);
void                   gio_file_stream_set_filename    (GioFileStream          *fstream,
						        const gchar            *filename);

GioFileHandle          gio_file_stream_get_handle      (GioFileStream          *fstream);
void                   gio_file_stream_set_handle      (GioFileStream          *fstream,
							GioFileHandle           handle);

GioFileFlags           gio_file_stream_get_file_flags  (GioFileStream          *fstream);
void                   gio_file_stream_set_file_flags  (GioFileStream          *fstream,
						        GioFileFlags            flags);

GioFilePermissionFlags gio_file_stream_get_permissions (GioFileStream          *file);
void                   gio_file_stream_set_permissions (GioFileStream          *file,
							GioFilePermissionFlags  permissions);

GQuark                 gio_file_error_get_quark        (void) G_GNUC_CONST;

void                   gio_set_file_error              (GioError      **error,
							GioErrorLevel   level,
							GioFileError    code,
							const gchar    *format,
							...) G_GNUC_PRINTF (4, 5);
void                   gio_set_file_error_from_errno   (GioError      **error,
							const gchar    *filename,
							gint            an_errno);


G_END_DECLS

#endif /* __GIO_FILE_STREAM_H__ */
