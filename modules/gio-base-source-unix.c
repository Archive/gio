/*
 * Gio Library: modules/gio-base-source-unix.c
 *
 * Copyright (c) 2005 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/* Common GSource for unix file/socket streams */

#include <config.h>

#include "gio-base-source-unix.h"

/* ******************** *
 *  Private Structures  *
 * ******************** */

struct _GioBaseSourceUnix
{
  GSource parent;

  GPollFD pollfd;
  GIOCondition cond;
};


/* ********************************** *
 *  GSource Implementation Functions  *
 * ********************************** */

static gboolean
unix_source_prepare (GSource *src,
		     gint    *timeout)
{
  *timeout = -1;

  return FALSE;
}

static gboolean
unix_source_check (GSource *src)
{
  GioBaseSourceUnix *source;

  source = (GioBaseSourceUnix *) src;

  return (source->pollfd.events & source->pollfd.revents);
}

static gboolean
unix_source_dispatch (GSource     *src,
		      GSourceFunc  callback,
		      gpointer     data)
{
  GioBaseSourceFunc func;
  GioBaseSourceUnix *source;

  if (!callback)
    g_error ("No callback given, this should not be possible.");

  func = (GioBaseSourceFunc) callback;
  source = (GioBaseSourceUnix *) src;

  return (*func) ((source->pollfd.revents & source->cond), data);
}

static void
unix_source_finalize (GSource *src)
{
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

/**
 * _gio_base_source_new_file:
 * @handle: 
 * @listen_for:
 * 
 * 
 * 
 * Returns: 
 * 
 * Since: 1.0
 **/
GSource *
_gio_base_source_unix_new (gint         fd,
			   GIOCondition listen_for)
{
  static GSourceFuncs source_funcs = {
    unix_source_prepare,
    unix_source_check,
    unix_source_dispatch,
    unix_source_finalize
  };
  GioBaseSourceUnix *source;

  source = (GioBaseSourceUnix *) g_source_new (&source_funcs,
					       sizeof (GioBaseSourceUnix));
  source->cond = listen_for;
  source->pollfd.fd = fd;
  source->pollfd.events = listen_for;
  g_source_add_poll ((GSource *) source, &source->pollfd);

  return (GSource *) source;
}

/**
 * _gio_base_source_get_condition:
 * @source:
 * 
 * 
 * 
 * Returns: 
 * 
 * Since: 1.0
 **/
GIOCondition
_gio_base_source_unix_get_condition (GioBaseSourceUnix *source)
{
  return source->cond;
}

/**
 * _gio_base_source_set_condition:
 * @source: 
 * @cond:
 * 
 * 
 * 
 * Returns: 
 * 
 * Since: 1.0
 **/
void
_gio_base_source_unix_set_condition (GioBaseSourceUnix *source,
				     GIOCondition       listen_for)
{
  g_source_remove_poll ((GSource *) source, &source->pollfd);
  source->cond = listen_for;
  source->pollfd.events = listen_for;
  g_source_add_poll ((GSource *) source, &source->pollfd);
}

