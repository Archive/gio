/*
 * Gio Library: gio/gio-file-stream.c
 *
 * Copyright (c) 2004 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <errno.h>

#include <glib/gi18n.h>

#include <gio/gio-type-builtins.h>

#include "gio-base-type-builtins.h"
#include "gio-base-private.h"

#include "gio-file-stream-private.h"


/* *************** *
 *  Documentation  *
 * *************** */

/**
 * GioFileStream:
 * 
 * The instance structure for GioFileStream objects. This structure contains no
 * public members.
 * 
 * Since: 1.0
 **/

/**
 * GioFileStreamClass:
 * 
 * The class structure for GioFileStream objects. This structure contains no
 * public members.
 * 
 * Since: 1.0
 **/

/**
 * GIO_FILE_STREAM:
 * @object: the pointer to cast.
 * 
 * Casts the #GioFileStream derived pointer at @object into a (GioFileStream*)
 * pointer. Depending on the current debugging level, this function may invoke
 * certain runtime checks to identify invalid casts.
 * 
 * Since: 1.0
 **/

/**
 * GIO_IS_FILE_STREAM:
 * @object: the pointer to check.
 * 
 * Checks whether @object is a #GioFileStream (or derived) instance.
 * 
 * Since: 1.0
 **/

/**
 * GIO_FILE_STREAM_CLASS:
 * @klass: the pointer to cast.
 * 
 * Casts a the #GioFileStreamClass derieved pointer at @klass into a
 * (GioFileStreamClass*) pointer.
 * 
 * Since: 1.0
 **/

/**
 * GIO_IS_FILE_STREAM_CLASS:
 * @klass: the pointer to check.
 * 
 * Checks whether @klass is a #GioFileStreamClass (or derived) class.
 * 
 * Since: 1.0
 **/

/**
 * GIO_FILE_STREAM_GET_CLASS:
 * @object: the pointer to check.
 * 
 * Retrieves a pointer to the #GioFileStreamClass for the #GioFileStream (or
 * derived) instance, @object.
 * 
 * Since: 1.0
 **/

/**
 * GIO_TYPE_FILE_STREAM:
 * 
 * The registered #GType of the #GioFileStream class.
 * 
 * Since: 1.0
 **/

/**
 * GioFileHandle:
 * 
 * A typedef of the system's file handle ("int fd" on Unix, "HANDLE" on Win32).
 * 
 * Since: 1.0
 **/

/**
 * GioFileFlags:
 * @GIO_FILE_READABLE: the file should be opened for reading.
 * @GIO_FILE_WRITABLE: the file should be opened for writing.
 * @GIO_FILE_CREATE: the file should be created if it does not exist already.
 * @GIO_FILE_NEW_ONLY: the file will be created and must not exist already.
 * @GIO_FILE_TRUNCATE: the file should be truncated when opened as writable.
 * @GIO_FILE_APPEND: the file should be appended to when opened as writable.
 * @GIO_FILE_READWRITE: a mask of the readable and writable flags.
 * 
 * A bitwise enumeration of the flags which can apply to a #GioFileStream.
 * 
 * Since: 1.0
 **/

/**
 * GIO_TYPE_FILE_FLAGS:
 * 
 * The registered #GType of the #GioFileFlags bitwise enumeration.
 * 
 * Since: 1.0
 **/

/**
 * GioFilePermissionFlags:
 * @GIO_FILE_PERMISSION_NONE: no permissions are set (the file is unusable).
 * @GIO_FILE_PERMISSION_OWNER_READABLE: the user who owns this file can read from it.
 * @GIO_FILE_PERMISSION_OWNER_WRITABLE: the user who owns this file can write to it.
 * @GIO_FILE_PERMISSION_OWNER_EXECUTABLE: the user who owns this file can run it as a program.
 * @GIO_FILE_PERMISSION_GROUP_READABLE: members of the group which owns this file can read from it.
 * @GIO_FILE_PERMISSION_GROUP_WRITABLE: members of the group which owns this file can write to it.
 * @GIO_FILE_PERMISSION_GROUP_EXECUTABLE: members of the group which owns this file can run it as a program.
 * @GIO_FILE_PERMISSION_OTHER_READABLE: other users can read from this file.
 * @GIO_FILE_PERMISSION_OTHER_WRITABLE: other users can write to this file.
 * @GIO_FILE_PERMISSION_OTHER_EXECUTABLE: other users can run this file as a program.
 * @GIO_FILE_PERMISSION_SET_USER: the setuid bit.
 * @GIO_FILE_PERMISSION_SET_GROUP: the setgid bit.
 * @GIO_FILE_PERMISSION_STICKY: the sticky bit.
 * 
 * A bitwise enumeration of the permission flags which can apply to a file.
 * 
 * Since: 1.0
 **/

/**
 * GIO_TYPE_FILE_PERMISSION_FLAGS:
 * 
 * The registered #GType of the #GioFilePermissionFlags bitwise enumeration.
 * 
 * Since: 1.0
 **/

/**
 * GioFileError:
 * @GIO_FILE_ERROR_ALREADY_EXISTS: the file already exists, but the NEW_ONLY flag is set.
 * @GIO_FILE_ERROR_DOESNT_EXIST: the file does not exist, and the CREATE flag is not set.
 * @GIO_FILE_ERROR_BAD_PATH: the given filename contains a directory which is really a file.
 * @GIO_FILE_ERROR_READ_PERMISSIONS: the user does not have permission to read this file.
 * @GIO_FILE_ERROR_WRITE_PERMISSIONS: the user does not have permission to write to this file.
 * @GIO_FILE_ERROR_FILE_BUSY: the file is an executable and it is running.
 * @GIO_FILE_ERROR_TOO_MANY_FILES: the OS cannot open any more files.
 * @GIO_FILE_ERROR_DISK_ERROR: there was a low-level (FS or hardware) error accessing the file.
 * @GIO_FILE_ERROR_INTERNAL: there is a bug in the object.
 * 
 * An enumeration of possible errors for file streams.
 * 
 * Since: 1.0
 **/

/**
 * GIO_FILE_ERROR:
 * 
 * The #GQuark error domain of the #GioFileError enumeration.
 * 
 * Since: 1.0
 **/

/**
 * GIO_TYPE_FILE_ERROR:
 * 
 * The registered #GType of the #GioFileError enumeration.
 * 
 * Since: 1.0
 **/


/* ********************** *
 *  Private Enumerations  *
 * ********************** */

enum
{
  PROP_0,
  PROP_HANDLE,
  PROP_FILENAME,
  PROP_FLAGS,
  PROP_PERMISSIONS,
  PROP_SHM
};


/* ********************* *
 *  Function Prototypes  *
 * ********************* */

/* GType Functions */
static void gio_file_stream_class_init   (GioFileStreamClass *class);
static void gio_file_stream_init         (GioFileStream      *fstream);

/* GObject Functions */
static void gio_file_stream_set_property (GObject      *object,
					  guint         property,
					  const GValue *value,
					  GParamSpec   *pspec);
static void gio_file_stream_get_property (GObject      *object,
					  guint         property,
					  GValue       *value,
					  GParamSpec   *pspec);
static void gio_file_stream_finalize     (GObject      *object);


/* ****************** *
 *  Global Variables  *
 * ****************** */

static GType gio_file_stream_type = G_TYPE_INVALID;
static gpointer gio_file_stream_parent_class;


/* ******************* *
 *  GType Declaration  *
 * ******************* */

void
gio_file_stream_register_type (GTypeModule *module)
{
  if (G_LIKELY (gio_file_stream_type == G_TYPE_INVALID))
    {
      static const GTypeInfo info = {
	sizeof (GioFileStreamClass),
	(GBaseInitFunc) NULL,
	(GBaseFinalizeFunc) NULL,
	(GClassInitFunc) gio_file_stream_class_init,
	(GClassFinalizeFunc) NULL,
	NULL,	/* class_data */
	sizeof (GioFileStream),
	0,	/* n_preallocs */
	(GInstanceInitFunc) gio_file_stream_init
      };

      gio_file_stream_type = g_type_module_register_type (module,
							  GIO_TYPE_STREAM,
							  "GioFileStream",
							  &info, 0);
    }
}

GType
gio_file_stream_get_type (void)
{
  return gio_file_stream_type;
}


/* ***************** *
 *  GType Functions  *
 * ***************** */

static void
gio_file_stream_class_init (GioFileStreamClass *class)
{
  GObjectClass *gobject_class;
  GioStreamClass *stream_class;

  gobject_class = G_OBJECT_CLASS (class);
  stream_class = GIO_STREAM_CLASS (class);

  gio_file_stream_parent_class = g_type_class_peek_parent (class);
  
  gobject_class->set_property = gio_file_stream_set_property;
  gobject_class->get_property = gio_file_stream_get_property;
  gobject_class->finalize = gio_file_stream_finalize;

  stream_class->get_flags = _gio_file_stream_impl_get_flags;
  stream_class->get_size = _gio_file_stream_impl_get_size;

  stream_class->open = _gio_file_stream_impl_open;
  stream_class->close = _gio_file_stream_impl_close;

  stream_class->freeze = _gio_file_stream_impl_freeze;
  stream_class->thaw = _gio_file_stream_impl_thaw;

  stream_class->read = _gio_file_stream_impl_read;

  stream_class->write = _gio_file_stream_impl_write;
  stream_class->flush = _gio_file_stream_impl_flush;

  stream_class->seek = _gio_file_stream_impl_seek;
  stream_class->tell = _gio_file_stream_impl_tell;

  stream_class->events_changed = _gio_file_stream_impl_events_changed;

#if (defined G_OS_UNIX)
  g_object_class_install_property (gobject_class,
				   PROP_HANDLE,
				   g_param_spec_int ("handle",
						     "Handle",
						     "The system file handle.",
						     -1, G_MAXINT, -1,
						     G_PARAM_READWRITE));
#elif (defined G_OS_WIN32)
  g_object_class_install_property (gobject_class,
				   PROP_HANDLE,
				   g_param_spec_pointer ("handle",
							 "Handle",
							 "The system file handle.",
							 G_PARAM_READWRITE));
#else
#error Unsupported operating system.
#endif /* G_OS_UNIX || G_OS_WIN32 || error */

  g_object_class_install_property (gobject_class,
				   PROP_FILENAME,
				   g_param_spec_string ("filename",
						        "Filename",
							"The path to a file on the local system, in UTF-8 encoding.",
							NULL,
							G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class,
				   PROP_FLAGS,
				   g_param_spec_flags ("file-flags",
						       "File Flags",
						       "The mode to use when opening this file.",
						       GIO_TYPE_FILE_FLAGS,
						       (GIO_FILE_READWRITE |
							GIO_FILE_CREATE),
						       G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class,
				   PROP_PERMISSIONS,
				   g_param_spec_flags ("file-permissions",
						       "File Permissions",
						       "The permissions on the file in question.",
						       GIO_TYPE_FILE_PERMISSION_FLAGS,
						       0,
						       G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class,
				   PROP_SHM,
				   g_param_spec_boolean ("shared-memory",
							 "Shared Memory",
							 "The filename and/or handle represent a shared memory object.",
							 FALSE,
							 G_PARAM_READWRITE));

  g_type_class_add_private (class, sizeof (GioFileStreamPrivate));
}

static void
gio_file_stream_init (GioFileStream *fstream)
{
  fstream->priv = G_TYPE_INSTANCE_GET_PRIVATE (fstream, GIO_TYPE_FILE_STREAM,
					       GioFileStreamPrivate);
  fstream->priv->flags = (GIO_FILE_READWRITE | GIO_FILE_CREATE);
  fstream->priv->permissions = 00644;
  fstream->priv->handle = -1;
}


/* ******************* *
 *  GObject Functions  *
 * ******************* */

static void
gio_file_stream_set_property (GObject      *object,
			      guint         property,
			      const GValue *value,
			      GParamSpec   *pspec)
{
  GioFileStreamPrivate *priv;

  priv = GIO_FILE_STREAM_GET_PRIVATE (object);

  g_return_if_fail (gio_stream_get_state (GIO_STREAM (object)) <= GIO_STREAM_STATE_CLOSED);

  switch (property)
    {
    case PROP_HANDLE:
#if (defined G_OS_UNIX)
      priv->handle = g_value_get_int (value);

      if (priv->handle >= 0)
#elif (defined G_OS_WIN32)
      priv->handle = g_value_get_pointer (value);

      if (priv->handle)
#endif /* !HAVE_WIN32 */
	gio_stream_state_event (GIO_STREAM (object), GIO_STREAM_STATE_OPEN);
      break;
    case PROP_FILENAME:
      g_free (priv->filename);
      priv->filename = g_value_dup_string (value);
      break;
    case PROP_FLAGS:
      priv->flags = g_value_get_flags (value);
      break;
    case PROP_PERMISSIONS:
      priv->permissions = g_value_get_flags (value);
      break;
    case PROP_SHM:
      priv->shm = (g_value_get_boolean (value) != FALSE);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}

static void
gio_file_stream_get_property (GObject    *object,
			      guint       property,
			      GValue     *value,
			      GParamSpec *pspec)
{
  GioFileStreamPrivate *priv;

  priv = GIO_FILE_STREAM_GET_PRIVATE (object);

  switch (property)
    {
    case PROP_HANDLE:
#if (defined G_OS_UNIX)
      g_value_set_int (value, priv->handle);
#elif (defined G_OS_WIN32)
      g_value_set_pointer (value, priv->handle);
#endif /* G_OS_UNIX || G_OS_WIN32 */
      break;
    case PROP_FILENAME:
      g_value_set_string (value, priv->filename);
      break;
    case PROP_FLAGS:
      g_value_set_flags (value, priv->flags);
      break;
    case PROP_PERMISSIONS:
      g_value_set_flags (value, priv->permissions);
      break;
    case PROP_SHM:
      g_value_set_boolean (value, priv->shm);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, pspec);
      break;
    }
}

static void
gio_file_stream_finalize (GObject *object)
{
  GioFileStreamPrivate *priv;

  priv = GIO_FILE_STREAM_GET_PRIVATE (object);

  g_free (priv->filename);

  if (G_OBJECT_CLASS (gio_file_stream_parent_class)->finalize != NULL)
    (*G_OBJECT_CLASS (gio_file_stream_parent_class)->finalize) (object);
}


/* ************************************************************************** *
 *  Public API                                                                *
 * ************************************************************************** */

/**
 * gio_file_stream_get_filename:
 * @fstream: the file stream to examine.
 *
 * Retrieves the name of the file that @fstream will read from/write to in
 * UTF-8 encoding.
 *
 * Returns: a pointer to a constant string in UTF-8 encoding. This string
 *  should not be modified or freed.
 *
 * Since: 1.0
 **/
G_CONST_RETURN gchar *
gio_file_stream_get_filename (GioFileStream *fstream)
{
  g_return_val_if_fail (GIO_IS_FILE_STREAM (fstream), NULL);

  return fstream->priv->filename;
}

/**
 * gio_file_stream_get_handle:
 * @fstream: the file stream to examine.
 *
 * Retrieves the underlying system handle for @fstream. On a UNIX system, this
 * is a file descriptor. On a Windows system, this is a HANDLE.
 *
 * Returns: an integer file descriptor.
 *
 * Since: 1.0
 **/
GioFileHandle
gio_file_stream_get_handle (GioFileStream *fstream)
{
  g_return_val_if_fail (GIO_IS_FILE_STREAM (fstream), GIO_FILE_HANDLE_INVALID);

  return fstream->priv->handle;
}

GioFileFlags
gio_file_stream_get_file_flags (GioFileStream *fstream)
{
  g_return_val_if_fail (GIO_IS_FILE_STREAM (fstream), 0);

  return fstream->priv->flags;
}

GioFilePermissionFlags
gio_file_stream_get_permissions (GioFileStream *fstream)
{
  g_return_val_if_fail (GIO_IS_FILE_STREAM (fstream), 0);

  return fstream->priv->permissions;
}

GQuark
gio_file_error_get_quark (void)
{
  static GQuark quark = 0;

  if (quark == 0)
    quark = g_quark_from_static_string ("gio-file-error");

  return quark;
}

/**
 * gio_set_file_error:
 * @error: a pointer to a location to store the error, or %NULL.
 * @code: the file error code to use.
 * @format: the format string for the error message.
 * @Varargs: the parameters for @format.
 * 
 * Convenience function to set a file error for an error return.
 * 
 * Since: 1.0
 **/
void
gio_set_file_error (GioError      **error,
		    GioErrorLevel   level,
		    GioFileError    code,
		    const gchar    *format,
		    ...)
{
  va_list args;

  g_return_if_fail (error == NULL || *error == NULL);
  g_return_if_fail (format == NULL || format[0] != '\0');

  if (error == NULL)
    return;

  *error = g_new (GioError, 1);

  (*error)->level = level;
  (*error)->error.domain = GIO_FILE_ERROR;
  (*error)->error.code = code;

  va_start (args, format);
  (*error)->error.message = g_strdup_vprintf (format, args);
  va_end (args);
}

/**
 * gio_set_file_error_from_errno:
 * @error: a pointer to a location to store the error, or %NULL.
 * @filename: the filename in question.
 * @an_errno: the errno to use.
 * 
 * Convenience function to set a file error for an error return based on a
 * filename and a standard errno.
 * 
 * Since: 1.0
 **/
void
gio_set_file_error_from_errno (GioError    **error,
			       const gchar  *filename,
			       gint          an_errno)
{
  GioFileError code;
  const gchar *format;
  GioErrorLevel level;

  g_return_if_fail (error == NULL || *error == NULL);
  g_return_if_fail (filename != NULL && filename[0] != '\0');

  if (error == NULL)
    return;

  switch (an_errno)
    {
    case EEXIST:
      code = GIO_FILE_ERROR_ALREADY_EXISTS;
      format = _("The file \"%s\" already exists.");
      level = GIO_ERROR_FATAL;
      break;
    case ENOENT:
      code = GIO_FILE_ERROR_DOESNT_EXIST;
      format = _("The file \"%s\" does not exist.");
      level = GIO_ERROR_FATAL;
      break;
    case ENOTDIR:
      code = GIO_FILE_ERROR_BAD_PATH;
      format = _("The file \"%s\" is not a valid filename.");
      level = GIO_ERROR_FATAL;
      break;
    case EACCES:
      code = GIO_FILE_ERROR_READ_PERMISSIONS;
      format = _("The permissions on the file \"%s\" do not allow you to read "
		 "from it.");
      level = GIO_ERROR_CRITICAL;
      break;
    case EROFS:
      code = GIO_FILE_ERROR_WRITE_PERMISSIONS;
      format = _("The permissions leading to the file \"%s\" do not allow you "
		 "to write to it.");
      level = GIO_ERROR_CRITICAL;
      break;
    case ETXTBSY:
      code = GIO_FILE_ERROR_FILE_BUSY;
      format = _("The file \"%s\" is a program which is currently running.");
      level = GIO_ERROR_FATAL;
      break;
    case EMFILE:
    case ENFILE:
      code = GIO_FILE_ERROR_TOO_MANY_FILES;
      format = _("There are too many files currently in use to open \"%s\".");
      level = GIO_ERROR_FATAL;
      break;
    case EIO:
      code = GIO_FILE_ERROR_DISK_ERROR;
      format = _("The hardware which \"%s\" is stored on encountered an error."
		 "Do not panic, try to save the file on another disk, use "
		 "\"fdisk\" to repair the disk, or open a backup copy if one "
		 "exists.");
      level = GIO_ERROR_FATAL;
      break;
    default:
      code = GIO_FILE_ERROR_INTERNAL;
      format = _("There was a bug uncovered while reading from \"%s\".");
      level = GIO_ERROR_FATAL;
      break;
    }

  *error = gio_error_new (level, GIO_FILE_ERROR, code, format, filename);
}
