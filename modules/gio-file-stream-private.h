/*
 * Gio Library: gio/gio-local-file-private.h
 *
 * Copyright (c) 2004 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GIO_FILE_STREAM_PRIVATE_H__
#define __GIO_FILE_STREAM_PRIVATE_H__ 1

#include "gio-file-stream.h"

G_BEGIN_DECLS

/* **************** *
 *  Private Macros  *
 * **************** */

#define GIO_FILE_STREAM_GET_PRIVATE(object)	(GIO_FILE_STREAM (object)->priv)


/* ******************** *
 *  Private Structures  *
 * ******************** */

struct _GioFileStreamPrivate
{
  GSource *source;
  gchar *filename;
  GioFileHandle handle;

  /* Flags */
  GioFileFlags flags:6;
  GioFilePermissionFlags permissions:12;
  gboolean shm:1;
};


/* ****************************** *
 *  Platform-Dependent Functions  *
 * ****************************** */

gssize         _gio_file_stream_impl_get_size       (GioStream  *stream);
GioStreamFlags _gio_file_stream_impl_get_flags      (GioStream  *stream);

void           _gio_file_stream_impl_freeze         (GioStream  *stream);
void           _gio_file_stream_impl_thaw           (GioStream  *stream);

gboolean       _gio_file_stream_impl_open           (GioStream  *stream,
						     GioError  **error);
gboolean       _gio_file_stream_impl_close          (GioStream  *stream,
						     GioError  **error);

gboolean       _gio_file_stream_impl_read           (GioStream  *stream,
						     GioBuffer **buffer,
						     gssize      read_size,
						     GioError  **error);

gboolean       _gio_file_stream_impl_write          (GioStream  *stream,
						     GioBuffer  *buffer,
						     gsize      *bytes_written,
						     GioError  **error);
void           _gio_file_stream_impl_flush          (GioStream  *stream);

gboolean       _gio_file_stream_impl_seek           (GioStream  *stream,
						     gssize      offset,
						     GSeekType   whence,
						     GioError  **error);
gssize         _gio_file_stream_impl_tell           (GioStream  *stream);

void           _gio_file_stream_impl_events_changed (GioStream    *stream,
						     GioEventType  old_events);

G_END_DECLS

#endif /* __GIO_FILE_STREAM_PRIVATE_H__ */
