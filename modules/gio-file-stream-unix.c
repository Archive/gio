/*
 * Gio Library: modules/gio-file-stream-unix.c
 *
 * Copyright (c) 2003-2005 James M. Cape.
 * All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

#include <glib/gi18n.h>

#include "gio-file-stream-private.h"


/* ******************* *
 *  Utility Functions  *
 * ******************* */

static void
handle_error (GioStream  *stream,
	      gint        an_errno,
	      GioError  **error)
{
  GioFileStreamPrivate *priv;
  GioError *err;

  priv = GIO_FILE_STREAM_GET_PRIVATE (stream);
  err = NULL;

  gio_set_file_error_from_errno (&err, priv->filename, an_errno);
  gio_stream_error_event_single (stream, err);
  if (error)
    *error = err;
  else
    gio_error_free (err);
}

/* ********************* *
 *  GioStream Functions  *
 * ********************* */
 
gssize
_gio_file_stream_impl_get_size (GioStream *stream)
{
  GioFileStreamPrivate *priv;
  struct stat st;
  gssize retval;

  priv = GIO_FILE_STREAM_GET_PRIVATE (stream);
  retval = -1;

  if (priv->handle > 0)
    {
      if (fstat (priv->handle, &st) == 0)
	retval = st.st_size;
    }
  else
    {
      gchar *filename;

      filename = g_filename_from_utf8 (priv->filename, -1, NULL, NULL, NULL);
    
      if (filename && stat (filename, &st) == 0)
	retval = st.st_size;

      g_free (filename);
    }

  return retval;
}

GioStreamFlags
_gio_file_stream_impl_get_flags (GioStream *stream)
{
  return (GIO_STREAM_TOPLEVEL | GIO_STREAM_READABLE |
	  GIO_STREAM_WRITABLE | GIO_STREAM_SEEKABLE);
}

void
_gio_file_stream_impl_freeze (GioStream *stream)
{
  
}

void
_gio_file_stream_impl_thaw (GioStream *stream)
{
  
}

gboolean
_gio_file_stream_impl_open (GioStream  *stream,
			    GioError  **error)
{
  GioFileStreamPrivate *priv;
  GioEventType events;
  gint flags, my_errno;
  gchar *filename;

  gio_stream_state_event (stream, GIO_STREAM_STATE_OPENING);

  priv = GIO_FILE_STREAM_GET_PRIVATE (stream);
  flags = 0;

  if (priv->flags & GIO_FILE_READABLE)
    flags |= O_RDONLY;

  if (priv->flags & GIO_FILE_WRITABLE)
    flags |= O_WRONLY;

  if (priv->flags & GIO_FILE_CREATE)
    flags |= O_CREAT;

  if (priv->flags & GIO_FILE_NEW_ONLY)
    flags |= O_EXCL;

  if (priv->flags & GIO_FILE_TRUNCATE)
    flags |= O_TRUNC;

  if (priv->flags & GIO_FILE_APPEND)
    flags |= O_APPEND;

  events = gio_stream_get_event_mask (stream);
  if (events & ~GIO_EVENT_STATE)
    flags |= O_NONBLOCK;

  filename = g_filename_from_utf8 (priv->filename, -1, NULL, NULL, NULL);
  errno = 0;
  priv->handle = open (filename, flags, priv->permissions);
  my_errno = errno;
  g_free (filename);

  if (priv->handle < 0)
    {
      handle_error (stream, my_errno, error);
      gio_stream_state_event (stream, GIO_STREAM_STATE_CLOSED);

      return FALSE;
    }

  gio_stream_state_event (stream, GIO_STREAM_STATE_OPEN);

  return TRUE;
}

gboolean
_gio_file_stream_impl_close (GioStream  *stream,
			     GioError  **error)
{
  GioFileStreamPrivate *priv;

  priv = GIO_FILE_STREAM_GET_PRIVATE (stream);

  if (priv->handle >= 0)
    {
      errno = 0;
      if (close (priv->handle) != 0)
        {
	  handle_error (stream, errno, error);
	  return FALSE;
	}

      priv->handle = -1;
    }

  return TRUE;
}

gboolean
_gio_file_stream_impl_read (GioStream  *stream,
			    GioBuffer **buffer,
			    gssize      read_size,
			    GioError  **error)
{
  GioFileStreamPrivate *priv;
  gpointer data;
  gint my_errno;
  gssize result;

  priv = GIO_FILE_STREAM_GET_PRIVATE (stream);

  g_assert (priv->handle >= 0);

  if (!(priv->flags & GIO_FILE_READABLE))
    {
      g_warning ("The file \"%s\" was not opened for reading.", priv->filename);
      return FALSE;
    }

  data = g_new (guchar, read_size + 1);

  errno = 0;
  result = read (priv->handle, data, read_size);
  my_errno = errno;

  /* Success */
  if (result > 0)
    {
      *buffer = gio_buffer_new ();
      *(((guchar *) data) + result) = 0;
      gio_buffer_take_data (*buffer, data, read_size + 1, result);
      return TRUE;
    }

  g_free (data);
  *buffer = NULL;

  /* Error */
  if (result == 0)
    gio_stream_eof_event (stream);
  else if (result != 0)
    handle_error (stream, my_errno, error);

  return FALSE;
}

gboolean
_gio_file_stream_impl_write (GioStream  *stream,
			     GioBuffer  *buffer,
			     gsize      *bytes_written,
			     GioError  **error)
{
  GioFileStreamPrivate *priv;
  gint my_errno;
  gssize result;
  gconstpointer data;
  gsize data_len;

  priv = GIO_FILE_STREAM_GET_PRIVATE (stream);

  g_assert (priv->handle >= 0);

  if (!(priv->flags & GIO_FILE_WRITABLE))
    {
      g_warning ("The file \"%s\" was not opened for writing.", priv->filename);
      return FALSE;
    }
  
  data = gio_buffer_get_data (buffer, NULL, &data_len);
  
  errno = 0;
  result = write (priv->handle, data, data_len);
  my_errno = errno;

  /* Success */
  if (result > 0)
    {
      if (bytes_written)
	*bytes_written = result;

      return TRUE;
    }

  if (bytes_written)
    *bytes_written = 0;

  if (result == 0)
    gio_stream_eof_event (stream);
  else
    handle_error (stream, my_errno, error);

  return FALSE;
}

void
_gio_file_stream_impl_flush (GioStream * stream)
{
  fdatasync (GIO_FILE_STREAM_GET_PRIVATE (stream)->handle);
}

gboolean
_gio_file_stream_impl_seek (GioStream  *stream,
			    gssize      offset,
			    GSeekType   whence,
			    GioError  **error)
{
  GioFileStreamPrivate *priv;
  gint my_errno;
  off_t result;

  priv = GIO_FILE_STREAM_GET_PRIVATE (stream);

  g_assert (priv->handle >= 0);

  switch (whence)
    {
    case G_SEEK_CUR:
    case G_SEEK_END:
      break;

    case G_SEEK_SET:
      if (offset == (off_t) gio_stream_tell (stream))
	return TRUE;
      break;

    default:
      g_assert_not_reached ();
      break;
    }

  errno = 0;
  result = lseek (priv->handle, (off_t) offset, whence);
  my_errno = errno;

  if (result < 0)
    {
      handle_error (stream, my_errno, error);
      return FALSE;
    }

  return TRUE;
}

gssize
_gio_file_stream_impl_tell (GioStream *stream)
{
  GioFileStreamPrivate *priv;

  priv = GIO_FILE_STREAM_GET_PRIVATE (stream);

  if (priv->handle)
    return lseek (priv->handle, 0, G_SEEK_CUR);

  return 0;
}

void
_gio_file_stream_impl_events_changed (GioStream    *stream,
				      GioEventType  old_events)
{
  
}
