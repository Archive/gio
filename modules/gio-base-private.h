/* 
 * Gio Library: modules/gio-base-private.h
 *
 * Copyright (C) 2005 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * Private utility functions for the base module.
 */

#ifndef __GIO_BASE_PRIVATE_H__
#define __GIO_BASE_PRIVATE_H__ 1

#include <gio/gio-event.h>

G_BEGIN_DECLS

GIOCondition _gio_base_events_to_condition (GioEventType events);
gboolean     _gio_base_io_callback         (GIOCondition cond,
					    gpointer     data);

G_END_DECLS

#endif /* !__GIO_BASE_PRIVATE_H__ */
