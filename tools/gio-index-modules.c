/* 
 * Gio Library: tools/gio-index-modules.c
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, read to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * Module-index updating tool.
 */

#include <config.h>

#include <string.h>

#include <libxml/xmlwriter.h>

#include <gio/gio-module.h>

#define MODULES_FILE	GIO_CACHE_DIR "/gio-modules.xml"


static gboolean use_stdout = FALSE;

static GOptionEntry options[] = {
  { "stdout", 's', 0, G_OPTION_ARG_NONE, &use_stdout,
    "Output to standard output", NULL },
  { NULL }
};

gint
main (gint   argc,
      gchar *argv[])
{
  GOptionContext *context;
  GError *error;
  gchar *path;
  const gchar *name;
  GDir *dir;
  xmlTextWriterPtr writer;
  
  context = g_option_context_new ("- Query Gio modules");
  g_option_context_add_main_entries (context, options, GETTEXT_PACKAGE);
  error = NULL;
  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_error ("Invalid command line option(s): %s", error->message);
      g_error_free (error);
      return -1;
    }

  g_type_init ();

  path = g_filename_from_utf8 (GIO_MODULES_DIR, -1, NULL, NULL, &error);
  if (!path)
    {
      g_error ("Could not convert modules directory location: %s",
	       error->message);
      return -1;
    }

  dir = g_dir_open (path, 0, &error);
  g_free (path);
  if (!dir)
    {
      g_error ("Could not list modules directory contents: %s",
	       error->message);
      return -1;
    }

  path = g_filename_from_utf8 (MODULES_FILE, -1, NULL, NULL, &error);
  writer = xmlNewTextWriterFilename (path, 0);
  if (!writer)
    {
      g_error ("Could not create XML writer: %s",
	       error->message);
      return -1;
    }
  g_free (path);

  xmlTextWriterWriteElement (writer, (const xmlChar *) "gio-modules", NULL);

  name = g_dir_read_name (dir);
  while (name)
    {
      if (g_str_has_prefix (name, "lib") &&
	  g_str_has_suffix (name, "." G_MODULE_SUFFIX))
	{
	  GioModule *module;
	  gchar *mname;

	  mname = g_strndup (name + 3,
			     strlen (name + 3) - strlen (G_MODULE_SUFFIX) - 1);

	  module = gio_module_get (mname);
	  if (module)
	    {
	      GSList *types;

	      xmlTextWriterWriteElement (writer,
					 (const xmlChar *) "module", NULL);
	      xmlTextWriterWriteAttribute (writer,
					   (const xmlChar *) "xml:id",
					   (const xmlChar *) mname);

	      types = gio_module_list_types (module);

	      while (types)
		{
		  xmlTextWriterWriteElement (writer,
					     (const xmlChar *) "gtype", NULL);
		  xmlTextWriterWriteAttribute (writer,
					       (const xmlChar *) "xml:id",
					       (const xmlChar *) types->data);
		  xmlTextWriterEndElement (writer);

		  g_free (types->data);
		  types = g_slist_delete_link (types, types);
		}

	      xmlTextWriterEndElement (writer);
	      g_type_module_unuse (G_TYPE_MODULE (module));
	    }
	  g_free (mname);
	}

      name = g_dir_read_name (dir);
    }
  xmlTextWriterEndElement (writer);
  xmlFreeTextWriter (writer);

  return 0;
}
