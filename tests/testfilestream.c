/* 
 * Gio Library: tests/testfilestream.c
 *
 * Copyright (C) 2004 James M. Cape
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, read to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * GioFileStream Test.
 */

#include <config.h>

#include <string.h>

#include <libxml/xmlwriter.h>

#include <gio/gio.h>

#include <modules/gio-file-stream.h>

#define DEFAULT_INPUT_FILE	"testfilestream.input"
#define DEFAULT_OUTPUT_FILE	NULL

static gchar *input_file = DEFAULT_INPUT_FILE;
static gchar *output_file = DEFAULT_OUTPUT_FILE;

static GOptionEntry options[] = {
  { "input-file", 'i', 0, G_OPTION_ARG_FILENAME, &input_file,
    "The input file", "M" },
  { "output-file", 'o', 0, G_OPTION_ARG_FILENAME, &output_file,
    "The output file", "M" },
  { NULL }
};

gint
main (gint   argc,
      gchar *argv[])
{
  GOptionContext *context;
  GioStream *instream, *outstream;
  GioBuffer *buffer;
  GError *gerror;
  GioError *error;

  context = g_option_context_new ("- Test GioFileStream");
  g_option_context_add_main_entries (context, options, GETTEXT_PACKAGE);
  error = NULL;
  if (!g_option_context_parse (context, &argc, &argv, &gerror))
    {
      g_error ("Invalid command line option(s): %s", gerror->message);
      g_error_free (gerror);
      return -1;
    }

  g_type_init ();

  instream = gio_stream_new ("GioFileStream",
			     "filename", input_file,
			     "file-flags", GIO_FILE_READABLE,
			     NULL);

  if (!gio_stream_open (instream, &error))
    g_error ("Could not open input file `%s': %s", input_file, error->error.message);

  if (output_file)
    outstream = gio_stream_new ("GioFileStream",
				"filename", output_file,
				"file-flags", (GIO_FILE_WRITABLE | GIO_FILE_CREATE),
				NULL);
  else
    {
      outstream = gio_stream_new ("GioFileStream",
				  "handle", 1,
				  NULL);
    }

  if (!gio_stream_open (outstream, &error))
    g_error ("Could not open output file `%s': %s", input_file, error->error.message);

  buffer = NULL;
  while (gio_stream_read (instream, &buffer, 1024, &error))
    {
      gsize bytes_written = 0;

      while (gio_stream_write (outstream, buffer, &bytes_written, &error))
	{
	  gio_buffer_remove (buffer, 0, bytes_written);
	}

      if (error)
	g_error ("Could not write output: %s", error->error.message);

      gio_buffer_free (buffer);
      buffer = NULL;
    }

  if (error)
    g_error ("Error reading input: %s", error->error.message);

  gio_stream_close (instream, NULL);

  return 0;
}
